# Description #

Graphic tool for creating configurations of experiments conducted by using Evolutionary Multi-Agent Systems.

# Wiki #
Find more info on [project wiki](https://bitbucket.org/aceagh/agent-configuration-editor/wiki/Home). 

# Authors #

Szymon Górowski, Robert Maguda, Paweł Topa, PhD