#!/bin/bash

git remote set-url --add --push origin $(git config --get remote.origin.url) 

git remote -v | grep bitbucket.org >> /dev/null

if [ $? != 0 ]; then
  printf "Please enter you bitbucket.org username:"
  read bitbucket_username
  git remote set-url --add --push origin "https://$bitbucket_username@bitbucket.org/aceagh/agent-configuration-editor.git"
fi

git remote -v | grep bitbucket.iisg.agh.edu.pl >> /dev/null

if [ $? != 0 ]; then
  printf "Please enter you bitbucket.iisg.agh.edu.pl username:"
  read iisg_username
  git remote set-url --add --push origin "https://$iisg_username@bitbucket.iisg.agh.edu.pl/scm/mgr2016/agent-configuration-editor.git"
fi