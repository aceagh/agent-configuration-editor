package pl.edu.agh.ace.core.configinterface.parser;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import pl.edu.agh.ace.core.configinterface.parser.exception.InvalidConfigInterfaceFormatException;

public class ConfigInterfaceParserTest {

	private final ConfigInterfaceParser parser = new ConfigInterfaceParser();

	@Test
	public void shouldParseConfigInterface() throws IOException, SQLException, InvalidConfigInterfaceFormatException {
		InputStream inputStream = this.getClass().getResourceAsStream("example_interface.ace");
		byte[] fileContent = IOUtils.toByteArray(inputStream);
		parser.parse(fileContent);
	}

}
