package pl.edu.agh.ace.svc.project.jto;

import java.util.List;

import pl.edu.agh.ace.svc.configinterface.jto.ConfigInterfaceJTO;

public class ProjectJTO {
	private Long id;

	private String name;

	private List<ConfigInterfaceJTO> configInterfaces;

	private String owner;

	public ProjectJTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ConfigInterfaceJTO> getConfigInterfaces() {
		return configInterfaces;
	}

	public void setConfigInterfaces(List<ConfigInterfaceJTO> configInterfaces) {
		this.configInterfaces = configInterfaces;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

}
