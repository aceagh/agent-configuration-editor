package pl.edu.agh.ace.svc.home.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.edu.agh.ace.core.project.dao.ProjectDao;
import pl.edu.agh.ace.svc.utils.UserInfo;
import pl.edu.agh.ace.svc.utils.UserInfoProvider;

@Controller
@RequestMapping("/home")
public class HomeController {
	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private UserInfoProvider userDataProvider;

	@RequestMapping(method = RequestMethod.GET)
	public String home(Model model) {
		UserInfo userData = userDataProvider.getUserData();
		model.addAttribute("projects", projectDao.findByOwner(userData.getEmail()));
		model.addAttribute("userData", userData);
		return "html/index";
	}

}
