package pl.edu.agh.ace.svc.configinterface.jto;

public class ConfigInterfaceJTO {
	private long id;

	private String name;

	private String content;

	private String state;

	public ConfigInterfaceJTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
