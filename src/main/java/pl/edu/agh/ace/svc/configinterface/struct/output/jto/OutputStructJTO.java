package pl.edu.agh.ace.svc.configinterface.struct.output.jto;

import java.util.List;

public class OutputStructJTO {

	private String name;

	private String description;

	private String helpUrl;

	private List<OutputStructFieldJTO> fields;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	public String getHelpUrl() {
		return helpUrl;
	}

	public List<OutputStructFieldJTO> getFields() {
		return fields;
	}

	public void setFields(List<OutputStructFieldJTO> fields) {
		this.fields = fields;
	}

}
