package pl.edu.agh.ace.svc.configinterface.function.jto;

import java.util.List;

public class FunctionJTO {

	private String name;

	private String description;

	private String helpUrl;

	private List<FunctionParameterJTO> parameters;

	private FunctionReturnDataJTO returnData;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	public String getHelpUrl() {
		return helpUrl;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<FunctionParameterJTO> getParameters() {
		return parameters;
	}

	public void setParameters(List<FunctionParameterJTO> parameters) {
		this.parameters = parameters;
	}

	public FunctionReturnDataJTO getReturnData() {
		return returnData;
	}

	public void setReturnData(FunctionReturnDataJTO returnData) {
		this.returnData = returnData;
	}

}
