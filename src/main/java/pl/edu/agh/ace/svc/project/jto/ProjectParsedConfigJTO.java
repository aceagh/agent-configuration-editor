package pl.edu.agh.ace.svc.project.jto;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.ace.svc.configinterface.jto.ConfigInterfaceParsedConfigJTO;

public class ProjectParsedConfigJTO {
	private Long id;

	private String name;

	private List<ConfigInterfaceParsedConfigJTO> configInterfacesParsed;

	private String owner;

	public ProjectParsedConfigJTO() {
		configInterfacesParsed = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ConfigInterfaceParsedConfigJTO> getConfigInterfacesParsed() {
		return configInterfacesParsed;
	}

	public void setConfigInterfacesParsed(List<ConfigInterfaceParsedConfigJTO> configInterfacesParsed) {
		this.configInterfacesParsed = configInterfacesParsed;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
}
