package pl.edu.agh.ace.svc.configinterface.struct.input.jto;

import java.util.List;

public class InputStructFieldJTO {

	private String type;

	private String description;

	private String helpUrl;

	private List<InputStructPathFieldJTO> fieldsOnPath;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	public String getHelpUrl() {
		return helpUrl;
	}

	public List<InputStructPathFieldJTO> getFieldsOnPath() {
		return fieldsOnPath;
	}

	public void setFieldsOnPath(List<InputStructPathFieldJTO> fieldsOnPath) {
		this.fieldsOnPath = fieldsOnPath;
	}

}
