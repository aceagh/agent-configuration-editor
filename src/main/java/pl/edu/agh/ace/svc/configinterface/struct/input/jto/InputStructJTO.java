package pl.edu.agh.ace.svc.configinterface.struct.input.jto;

import java.util.List;

public class InputStructJTO {

	private String name;

	private List<InputStructFieldJTO> fields;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<InputStructFieldJTO> getFields() {
		return fields;
	}

	public void setFields(List<InputStructFieldJTO> fields) {
		this.fields = fields;
	}

}
