package pl.edu.agh.ace.svc.jto;

public class ItemIdJTO {
	Long id;

	public ItemIdJTO(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
