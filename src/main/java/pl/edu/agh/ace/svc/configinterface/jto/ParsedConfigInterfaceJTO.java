package pl.edu.agh.ace.svc.configinterface.jto;

import java.util.List;

import pl.edu.agh.ace.svc.configinterface.function.jto.FunctionJTO;
import pl.edu.agh.ace.svc.configinterface.struct.input.jto.InputStructJTO;
import pl.edu.agh.ace.svc.configinterface.struct.output.jto.OutputStructJTO;

public class ParsedConfigInterfaceJTO {
	private List<FunctionJTO> functions;

	private List<InputStructJTO> inputStructs;

	private List<OutputStructJTO> outputStructs;

	public ParsedConfigInterfaceJTO() {

	}

	public List<FunctionJTO> getFunctions() {
		return functions;
	}

	public void setFunctions(List<FunctionJTO> functions) {
		this.functions = functions;
	}

	public List<InputStructJTO> getInputStructs() {
		return inputStructs;
	}

	public void setInputStructs(List<InputStructJTO> inputStructs) {
		this.inputStructs = inputStructs;
	}

	public List<OutputStructJTO> getOutputStructs() {
		return outputStructs;
	}

	public void setOutputStructs(List<OutputStructJTO> outputStructs) {
		this.outputStructs = outputStructs;
	}

}
