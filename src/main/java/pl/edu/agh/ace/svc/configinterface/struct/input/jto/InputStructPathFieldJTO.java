package pl.edu.agh.ace.svc.configinterface.struct.input.jto;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class InputStructPathFieldJTO {

	private String name;

	private Integer listSize;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getListSize() {
		return listSize;
	}

	public void setListSize(Integer listSize) {
		this.listSize = listSize;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		InputStructPathFieldJTO that = (InputStructPathFieldJTO) obj;
		return new EqualsBuilder().append(name, that.name).append(listSize, that.listSize).isEquals();
	}

}
