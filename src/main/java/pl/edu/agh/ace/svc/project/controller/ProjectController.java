package pl.edu.agh.ace.svc.project.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.edu.agh.ace.core.project.assembler.ProjectAssembler;
import pl.edu.agh.ace.core.project.dao.ProjectDao;
import pl.edu.agh.ace.core.project.model.Project;
import pl.edu.agh.ace.svc.configinterface.jto.ConfigInterfaceJTO;
import pl.edu.agh.ace.svc.jto.ItemIdJTO;
import pl.edu.agh.ace.svc.jto.ItemNameIdJTO;
import pl.edu.agh.ace.svc.project.jto.ProjectJTO;
import pl.edu.agh.ace.svc.project.jto.ProjectParsedConfigJTO;
import pl.edu.agh.ace.svc.utils.UserInfo;
import pl.edu.agh.ace.svc.utils.UserInfoProvider;

@Controller
@RequestMapping(value = "/project")
public class ProjectController {

	@Autowired
	private UserInfoProvider userDataProvider;

	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private ProjectAssembler projectAssembler;

	@RequestMapping(value = "/createEmpty", method = RequestMethod.POST)
	public @ResponseBody ProjectParsedConfigJTO createEmpty(@RequestParam("project-name") String projectName) {
		Project project = new Project();
		project.setName(projectName);
		project.setOwner(userDataProvider.getUserData().getEmail());
		projectDao.save(project);
		return projectAssembler.convert(project);
	}

	@RequestMapping(value = "/get", method = RequestMethod.POST)
	public @ResponseBody ProjectParsedConfigJTO get(@RequestParam("project-id") long id) {
		return projectAssembler.convert(projectDao.findById(id));
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody ProjectParsedConfigJTO save(@RequestBody ProjectJTO projectJTO) {
		projectDao.save(projectAssembler.convert(projectJTO));
		return projectAssembler.convert(projectDao.findById(projectJTO.getId()));
	}

	@RequestMapping(value = "/import", method = RequestMethod.POST)
	public @ResponseBody ProjectParsedConfigJTO importProject(@RequestBody ProjectJTO projectJTO) {
		Project project = new Project();
		project.setName(projectJTO.getName());
		project.setOwner(userDataProvider.getUserData().getEmail());
		projectDao.save(project);

		projectJTO.setId(project.getId());
		projectJTO.setOwner(project.getOwner());

		for (ConfigInterfaceJTO configInterfaceJTO : projectJTO.getConfigInterfaces()) {
			configInterfaceJTO.setId(0);
		}

		projectDao.save(projectAssembler.convert(projectJTO));
		return projectAssembler.convert(projectDao.findById(projectJTO.getId()));
	}

	@RequestMapping(value = "/rename", method = RequestMethod.POST)
	public @ResponseBody ItemNameIdJTO rename(@RequestParam("project-id") String id,
			@RequestParam("new-name") String newName) {
		Project project = projectDao.findById(Long.parseLong(id));

		if (project == null || project.getOwner().compareTo(userDataProvider.getUserData().getEmail()) != 0) {
			return null;
		}

		project.setName(newName);
		projectDao.save(project);

		return new ItemNameIdJTO(project.getName(), project.getId());
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody ItemIdJTO delete(@RequestParam("project-id") String id) {
		Project project = projectDao.findById(Long.parseLong(id));

		if (project == null || project.getOwner().compareTo(userDataProvider.getUserData().getEmail()) != 0) {
			return null;
		}

		projectDao.delete(project);
		return new ItemIdJTO(project.getId());
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public @ResponseBody List<ItemNameIdJTO> getProjectList() {
		UserInfo userData = userDataProvider.getUserData();
		List<ItemNameIdJTO> itemNameIdList = new ArrayList<>();
		for (Project project : projectDao.findByOwner(userData.getEmail())) {
			itemNameIdList.add(new ItemNameIdJTO(project.getName(), project.getId()));
		}
		return itemNameIdList;
	}
}
