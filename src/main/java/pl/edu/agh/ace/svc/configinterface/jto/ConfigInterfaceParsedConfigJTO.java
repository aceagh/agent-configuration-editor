package pl.edu.agh.ace.svc.configinterface.jto;

import pl.edu.agh.ace.core.configinterface.model.ConfigInterface;

public class ConfigInterfaceParsedConfigJTO {
	private long id;

	private String name;

	private String content;

	private String state;

	private ParsedConfigInterfaceJTO parsedConfigInterface;

	public ConfigInterfaceParsedConfigJTO() {
	}

	public ConfigInterfaceParsedConfigJTO(ConfigInterface configInterface,
			ParsedConfigInterfaceJTO parsedConfigInterface) {
		this.id = configInterface.getId();
		this.name = configInterface.getName();
		this.content = new String(configInterface.getContent());
		this.state = configInterface.getState() == null ? "" : new String(configInterface.getState());
		this.parsedConfigInterface = parsedConfigInterface;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public ParsedConfigInterfaceJTO getParsedConfigInterface() {
		return parsedConfigInterface;
	}

	public void setParsedConfigInterface(ParsedConfigInterfaceJTO parsedConfigInterface) {
		this.parsedConfigInterface = parsedConfigInterface;
	}

}
