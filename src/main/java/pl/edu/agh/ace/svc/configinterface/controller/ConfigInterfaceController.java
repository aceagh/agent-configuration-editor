package pl.edu.agh.ace.svc.configinterface.controller;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import pl.edu.agh.ace.core.configinterface.dao.ConfigInterfaceDao;
import pl.edu.agh.ace.core.configinterface.model.ConfigInterface;
import pl.edu.agh.ace.core.configinterface.parser.ConfigInterfaceParser;
import pl.edu.agh.ace.core.configinterface.parser.exception.InvalidConfigInterfaceFormatException;
import pl.edu.agh.ace.svc.configinterface.jto.ConfigInterfaceParsedConfigJTO;
import pl.edu.agh.ace.svc.configinterface.jto.ParsedConfigInterfaceJTO;
import pl.edu.agh.ace.svc.jto.ItemIdJTO;
import pl.edu.agh.ace.svc.jto.ItemNameIdJTO;

@Controller
@RequestMapping(value = "/configInterface")
public class ConfigInterfaceController {

	@Autowired
	private ConfigInterfaceDao configInterfaceDao;

	@Autowired
	private ConfigInterfaceParser parser;

	@RequestMapping(value = "/parse", method = RequestMethod.POST)
	public ResponseEntity<?> parse(@RequestParam("uploadfile") MultipartFile uploadfile,
			@RequestParam("project-id") long projectId) {
		ConfigInterface configInterface = new ConfigInterface();
		ParsedConfigInterfaceJTO parsedConfigInterfaceJTO = null;
		try {
			configInterface.setName(uploadfile.getOriginalFilename());
			configInterface.setContent(uploadfile.getBytes());

			parsedConfigInterfaceJTO = parser.parse(configInterface.getContent());
		} catch (SQLException e) {
			// TODO handle exception
		} catch (IOException e) {
			// TODO handle exception
		} catch (InvalidConfigInterfaceFormatException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

		return new ResponseEntity<>(new ConfigInterfaceParsedConfigJTO(configInterface, parsedConfigInterfaceJTO),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/rename", method = RequestMethod.POST)
	public @ResponseBody ItemNameIdJTO renameInterface(@RequestParam("interface-id") String id,
			@RequestParam("new-name") String newName) {
		ConfigInterface configInterface = configInterfaceDao.findById(Long.parseLong(id));
		if (configInterface == null) {
			return null;
		}
		configInterface.setName(newName);
		configInterfaceDao.save(configInterface);
		return new ItemNameIdJTO(configInterface.getName(), configInterface.getId());
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody ItemIdJTO deleteInterface(@RequestParam("interface-id") String id) {
		ConfigInterface configInterface = configInterfaceDao.findById(Long.parseLong(id));
		if (configInterface == null) {
			return null;
		}
		configInterfaceDao.delete(configInterface);
		return new ItemIdJTO(configInterface.getId());
	}
}
