package pl.edu.agh.ace.svc.utils;

import java.util.Map;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Service
public class UserInfoProvider {

	private Gson gson;

	public UserInfoProvider() {
		gson = new Gson();
	}

	public UserInfo getUserData() {
		UserInfo userData = new UserInfo();
		JsonObject jsonObject = getJsonObject();
		userData.setEmail(jsonObject.get("email").getAsString());
		userData.setDisplayName(jsonObject.get("name").getAsString());
		userData.setImageUrl(changeRequestImageSize(jsonObject.get("picture").getAsString()));
		return userData;
	}

	private JsonObject getJsonObject() {
		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();
		@SuppressWarnings("unchecked")
		Map<String, Object> authenticationDetails = (Map<String, Object>) authentication.getUserAuthentication()
				.getDetails();
		JsonObject json = gson.toJsonTree(authenticationDetails).getAsJsonObject();
		return json;
	}

	private String changeRequestImageSize(String imageUrl) {
		return imageUrl.replaceAll("\\?sz=[0-9]+", "?sz=150");
	}
}
