package pl.edu.agh.ace.core.configinterface.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.ace.core.configinterface.model.ConfigInterface;

@Transactional
public interface ConfigInterfaceDao extends CrudRepository<ConfigInterface, Long> {
	@Override
	public List<ConfigInterface> findAll();

	public ConfigInterface findById(long id);
}
