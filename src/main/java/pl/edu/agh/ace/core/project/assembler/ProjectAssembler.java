package pl.edu.agh.ace.core.project.assembler;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.edu.agh.ace.core.configinterface.model.ConfigInterface;
import pl.edu.agh.ace.core.configinterface.parser.ConfigInterfaceParser;
import pl.edu.agh.ace.core.configinterface.parser.exception.InvalidConfigInterfaceFormatException;
import pl.edu.agh.ace.core.project.model.Project;
import pl.edu.agh.ace.svc.configinterface.jto.ConfigInterfaceJTO;
import pl.edu.agh.ace.svc.configinterface.jto.ConfigInterfaceParsedConfigJTO;
import pl.edu.agh.ace.svc.configinterface.jto.ParsedConfigInterfaceJTO;
import pl.edu.agh.ace.svc.project.jto.ProjectJTO;
import pl.edu.agh.ace.svc.project.jto.ProjectParsedConfigJTO;

@Component
public class ProjectAssembler {

	@Autowired
	private ConfigInterfaceParser parser;

	public ProjectParsedConfigJTO convert(Project project) {
		ProjectParsedConfigJTO parsedProjectJTO = new ProjectParsedConfigJTO();

		parsedProjectJTO.setId(project.getId());
		parsedProjectJTO.setName(project.getName());
		parsedProjectJTO.setOwner(project.getOwner());

		for (ConfigInterface configInterface : project.getConfigInterfaces()) {
			ParsedConfigInterfaceJTO parsedConfigInterfaceJTO = null;
			try {
				parsedConfigInterfaceJTO = parser.parse(configInterface.getContent());
			} catch (SQLException e) {
				// TODO handle exception
			} catch (IOException e) {
				// TODO handle exception
			} catch (InvalidConfigInterfaceFormatException e) {
				// TODO return exception message to user
				System.err.println(e.getMessage());
			}
			ConfigInterfaceParsedConfigJTO configInterfaceJTO = new ConfigInterfaceParsedConfigJTO(configInterface,
					parsedConfigInterfaceJTO);
			parsedProjectJTO.getConfigInterfacesParsed().add(configInterfaceJTO);
		}
		return parsedProjectJTO;
	}

	public Project convert(ProjectJTO projectJTO) {
		Project project = new Project();
		project.setId(projectJTO.getId());
		project.setName(projectJTO.getName());
		project.setOwner(projectJTO.getOwner());
		for (ConfigInterfaceJTO configInterfaceJTO : projectJTO.getConfigInterfaces()) {
			ConfigInterface configInterface = new ConfigInterface();
			configInterface.setId(configInterfaceJTO.getId());
			configInterface.setName(configInterfaceJTO.getName());
			configInterface.setState(configInterfaceJTO.getState().getBytes());
			configInterface.setContent(configInterfaceJTO.getContent().getBytes());
			project.getConfigInterfaces().add(configInterface);
		}
		return project;
	}
}
