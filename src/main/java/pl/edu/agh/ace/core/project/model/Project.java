package pl.edu.agh.ace.core.project.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import pl.edu.agh.ace.core.configinterface.model.ConfigInterface;

@Entity
@Table(name = "Projects")
@Component
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	private String name;

	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "projectId", referencedColumnName = "id")
	private List<ConfigInterface> configInterfaces;

	@NotNull
	private String owner;

	public Project() {
		configInterfaces = new ArrayList<>();
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ConfigInterface> getConfigInterfaces() {
		return configInterfaces;
	}

	public void setConfigInterfaces(List<ConfigInterface> configInterfaces) {
		this.configInterfaces = configInterfaces;
	}

	public void addConfigInterfaces(ConfigInterface configInterfaces) {
		this.configInterfaces.add(configInterfaces);
	}
}
