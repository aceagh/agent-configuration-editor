package pl.edu.agh.ace.core.configinterface.parser.utils;

import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.ace.svc.configinterface.function.jto.FunctionParameterJTO;
import pl.edu.agh.ace.svc.configinterface.function.jto.FunctionReturnDataJTO;
import pl.edu.agh.ace.svc.configinterface.struct.input.jto.InputStructFieldJTO;
import pl.edu.agh.ace.svc.configinterface.struct.output.jto.OutputStructFieldJTO;

public class ConfigInterfaceSection {

	private List<FunctionParameterJTO> parameters;

	private FunctionReturnDataJTO returnData;

	private List<OutputStructFieldJTO> outputStructFields;

	private InputStructFieldJTO inputStructField;

	private String description;

	private String helpUrl;

	public ConfigInterfaceSection() {
		this.parameters = new LinkedList<>();
		this.outputStructFields = new LinkedList<>();
	}

	public String getDescription() {
		return description;
	}

	public String getHelpUrl() {
		return helpUrl;
	}

	public FunctionReturnDataJTO getReturnData() {
		return returnData;
	}

	public List<FunctionParameterJTO> getParameters() {
		return parameters;
	}

	public List<OutputStructFieldJTO> getOutputStructFields() {
		return outputStructFields;
	}

	public InputStructFieldJTO getInputStructField() {
		return inputStructField;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	public void setReturnData(FunctionReturnDataJTO returnData) {
		this.returnData = returnData;
	}

	public void setInputStructField(InputStructFieldJTO inputStructField) {
		this.inputStructField = inputStructField;
	}

	public void addParameter(FunctionParameterJTO parameter) {
		this.parameters.add(parameter);
	}

	public void addOutputStructField(OutputStructFieldJTO field) {
		this.outputStructFields.add(field);
	}

	public boolean containsParameter(String paramName) {
		return parameters.stream().anyMatch(param -> param.getName().equals(paramName));
	}

	public boolean containsDescription() {
		return description != null;
	}

	public boolean containsHelpUrl() {
		return helpUrl != null;
	}

	public boolean containsReturnData() {
		return returnData != null;
	}

	public boolean containsInputStructField() {
		return inputStructField != null;
	}

	public boolean containsOutputStructField(String fieldName) {
		return outputStructFields.stream().anyMatch(field -> field.getName().equals(fieldName));
	}

	public boolean containsOnlyFunctionDefinition() {
		return inputStructField == null && outputStructFields.isEmpty();
	}

	public boolean containsOnlyOutputStructDefinition() {
		return inputStructField == null && parameters.isEmpty() && returnData == null;
	}

	public boolean containsOnlyInputStructDefinition() {
		return parameters.isEmpty() && returnData == null && outputStructFields.isEmpty() && description == null;
	}

	public boolean containsAnyDefinition() {
		return !parameters.isEmpty() || returnData != null || !outputStructFields.isEmpty() || inputStructField != null
				|| description != null || helpUrl != null;
	}

	public boolean parametersIn(List<String> declaredParameters) {
		return parameters.stream().allMatch(param -> declaredParameters.contains(param.getName()));
	}

	public boolean fieldsIn(List<String> declaredFields) {
		return outputStructFields.stream().allMatch(field -> declaredFields.contains(field.getName()));
	}

	public void addMissingParameters(List<String> declaredParameters) {
		for (String declaredParameter : declaredParameters) {
			if (parameters.stream().noneMatch(param -> param.getName().equals(declaredParameter))) {
				FunctionParameterJTO parameter = new FunctionParameterJTO();
				parameter.setName(declaredParameter);
				parameters.add(parameter);
			}
		}
	}

	public void addMissingFields(List<String> declaredFields) {
		for (String declaredField : declaredFields) {
			if (outputStructFields.stream().noneMatch(field -> field.getName().equals(declaredField))) {
				OutputStructFieldJTO field = new OutputStructFieldJTO();
				field.setName(declaredField);
				outputStructFields.add(field);
			}
		}
	}

}
