package pl.edu.agh.ace.core.configinterface.parser.exception;

public class InvalidConfigInterfaceFormatException extends Exception {

	private static final long serialVersionUID = 1L;

	private Integer lineNumber;

	public InvalidConfigInterfaceFormatException(Integer lineNumber, String message) {
		super(message);
		this.lineNumber = lineNumber;
	}

	@Override
	public String getMessage() {
		return "Line " + lineNumber + ": " + super.getMessage();
	}
}
