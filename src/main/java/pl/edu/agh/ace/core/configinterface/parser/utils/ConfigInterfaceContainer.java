package pl.edu.agh.ace.core.configinterface.parser.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.ace.svc.configinterface.function.jto.FunctionJTO;
import pl.edu.agh.ace.svc.configinterface.struct.input.jto.InputStructFieldJTO;
import pl.edu.agh.ace.svc.configinterface.struct.input.jto.InputStructJTO;
import pl.edu.agh.ace.svc.configinterface.struct.input.jto.InputStructPathFieldJTO;
import pl.edu.agh.ace.svc.configinterface.struct.output.jto.OutputStructJTO;

public class ConfigInterfaceContainer {

	private String globalHelpUrl;

	private Map<String, FunctionJTO> functionContainer;

	private Map<String, InputStructJTO> inputStructContainer;

	private Map<String, OutputStructJTO> outputStructContainer;

	public ConfigInterfaceContainer() {
		this.functionContainer = new HashMap<>();
		this.inputStructContainer = new HashMap<>();
		this.outputStructContainer = new HashMap<>();
	}

	public List<FunctionJTO> getFunctions() {
		Collection<FunctionJTO> functions = functionContainer.values();
		setFunctionsHelpUrl(functions);
		return new ArrayList<>(functions);
	}

	public List<InputStructJTO> getInputStructs() {
		Collection<InputStructJTO> inputStructs = inputStructContainer.values();
		setInputStructsHelpUrl(inputStructs);
		return new ArrayList<>(inputStructs);
	}

	public List<OutputStructJTO> getOutputStructs() {
		Collection<OutputStructJTO> outputStructs = outputStructContainer.values();
		setOutputStructsHelpUrl(outputStructs);
		return new ArrayList<>(outputStructs);
	}

	public InputStructJTO getInputStruct(String name) {
		return inputStructContainer.get(name);
	}

	public void addFunction(FunctionJTO function) {
		functionContainer.put(function.getName(), function);
	}

	public void addOutputStruct(OutputStructJTO struct) {
		outputStructContainer.put(struct.getName(), struct);
	}

	public void addInputStructField(String structName, InputStructFieldJTO field) {
		InputStructJTO struct = inputStructContainer.get(structName);
		if (struct == null) {
			return;
		}
		struct.getFields().add(field);
	}

	public void addInputStruct(InputStructJTO struct) {
		inputStructContainer.put(struct.getName(), struct);
	}

	public void setGlobalHelpUrl(String helpUrl) {
		this.globalHelpUrl = helpUrl;
	}

	public boolean containsFunction(String name) {
		return functionContainer.containsKey(name);
	}

	public boolean containsOutputStruct(String name) {
		return outputStructContainer.containsKey(name);
	}

	public boolean containsInputStruct(String name) {
		return inputStructContainer.containsKey(name);
	}

	public boolean containsGlobalHelpUrl() {
		return globalHelpUrl != null;
	}

	public boolean containsInputStructField(String structName, List<InputStructPathFieldJTO> fieldsOnPathJTO) {
		InputStructJTO struct = inputStructContainer.get(structName);
		if (struct == null) {
			return false;
		}
		return struct.getFields().stream().anyMatch(field -> field.getFieldsOnPath().equals(fieldsOnPathJTO));
	}

	private void setFunctionsHelpUrl(Collection<FunctionJTO> functions) {
		if (globalHelpUrl != null) {
			for (FunctionJTO function : functions) {
				if (function.getHelpUrl() == null) {
					function.setHelpUrl(globalHelpUrl);
				}
			}
		}
	}

	private void setInputStructsHelpUrl(Collection<InputStructJTO> inputStructs) {
		if (globalHelpUrl != null) {
			for (InputStructJTO inputStruct : inputStructs) {
				for (InputStructFieldJTO inputStructField : inputStruct.getFields()) {
					if (inputStructField.getHelpUrl() == null) {
						inputStructField.setHelpUrl(globalHelpUrl);
					}
				}
			}
		}
	}

	private void setOutputStructsHelpUrl(Collection<OutputStructJTO> outputStructs) {
		if (globalHelpUrl != null) {
			for (OutputStructJTO outputStruct : outputStructs) {
				if (outputStruct.getHelpUrl() == null) {
					outputStruct.setHelpUrl(globalHelpUrl);
				}
			}
		}
	}
}
