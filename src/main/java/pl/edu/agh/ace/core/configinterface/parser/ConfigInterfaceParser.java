package pl.edu.agh.ace.core.configinterface.parser;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;

import pl.edu.agh.ace.core.configinterface.parser.exception.InvalidConfigInterfaceFormatException;
import pl.edu.agh.ace.core.configinterface.parser.utils.ConfigInterfaceContainer;
import pl.edu.agh.ace.core.configinterface.parser.utils.ConfigInterfaceSection;
import pl.edu.agh.ace.svc.configinterface.function.jto.FunctionJTO;
import pl.edu.agh.ace.svc.configinterface.function.jto.FunctionParameterJTO;
import pl.edu.agh.ace.svc.configinterface.function.jto.FunctionReturnDataJTO;
import pl.edu.agh.ace.svc.configinterface.jto.ParsedConfigInterfaceJTO;
import pl.edu.agh.ace.svc.configinterface.struct.input.jto.InputStructFieldJTO;
import pl.edu.agh.ace.svc.configinterface.struct.input.jto.InputStructJTO;
import pl.edu.agh.ace.svc.configinterface.struct.input.jto.InputStructPathFieldJTO;
import pl.edu.agh.ace.svc.configinterface.struct.output.jto.OutputStructFieldJTO;
import pl.edu.agh.ace.svc.configinterface.struct.output.jto.OutputStructJTO;

@Component
public class ConfigInterfaceParser {

	private static final String helpIdentifier = "@help";

	private static final String descriptionIdentifier = "@description";

	private static final String parameterIdentifier = "@param";

	private static final String returnIdentifier = "@return";

	private static final String inputFieldIdentifier = "@inputField";

	private static final String outputFieldIdentifier = "@outputField";

	private static final String globalHelpIdentifier = "help";

	private static final String functionIdentifier = "func";

	private static final String structIdentifier = "struct";

	private static final String inputStructIdentifier = "input";

	private static final String outputStructIdentifier = "output";

	private static final List<String> commentIdentifiers = ImmutableList.of(descriptionIdentifier, parameterIdentifier,
			returnIdentifier, inputFieldIdentifier, outputFieldIdentifier);

	private Integer lineNumber;

	public ParsedConfigInterfaceJTO parse(byte[] configInterface)
			throws SQLException, IOException, InvalidConfigInterfaceFormatException {
		ConfigInterfaceContainer configInterfaceContainer = new ConfigInterfaceContainer();
		ConfigInterfaceSection configInterfaceSection = new ConfigInterfaceSection();
		String line;
		lineNumber = 0;
		BufferedReader configInterfaceReader = bufferedReader(configInterface);
		while ((line = configInterfaceReader.readLine()) != null) {
			lineNumber++;
			line = removeExcessWhiteSpaces(line);
			if (line.startsWith("//")) {
				parseComment(line, configInterfaceSection);
			} else if (parseDeclarationIfPresent(line, configInterfaceSection, configInterfaceContainer)) {
				configInterfaceSection = new ConfigInterfaceSection();
			}
		}
		configInterfaceReader.close();
		return createConfigInterfaceModel(configInterfaceContainer);
	}

	private void parseComment(String line, ConfigInterfaceSection configInterfaceSection)
			throws InvalidConfigInterfaceFormatException {
		line = removeCommentPrefix(line);
		String[] parts = line.split(" ");
		String identifier = parts[0];
		if (matchesLengthAndIdentifier(parts, 2, descriptionIdentifier)) {
			createDescription(configInterfaceSection, parts);
		} else if (matchesLengthAndIdentifier(parts, 2, helpIdentifier)) {
			createHelpUrl(configInterfaceSection, parts);
		} else if (matchesLengthAndIdentifier(parts, 4, parameterIdentifier)) {
			createParameter(configInterfaceSection, parts);
		} else if (matchesLengthAndIdentifier(parts, 3, returnIdentifier)) {
			createReturnData(configInterfaceSection, parts);
		} else if (matchesLengthAndIdentifier(parts, 3, inputFieldIdentifier)) {
			createInputStructField(configInterfaceSection, parts);
		} else if (matchesLengthAndIdentifier(parts, 4, outputFieldIdentifier)) {
			createOutputStructField(configInterfaceSection, parts);
		} else if (parts.length > 0 && commentIdentifiers.contains(identifier)) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Invalid definition of \"" + identifier + "\"");
		}
	}

	private boolean parseDeclarationIfPresent(String line, ConfigInterfaceSection configInterfaceSection,
			ConfigInterfaceContainer configInterfaceContainer) throws InvalidConfigInterfaceFormatException {
		String[] parts = line.split(" ");
		if (matchesLengthAndIdentifier(parts, 2, functionIdentifier)) {
			createFunction(configInterfaceContainer, configInterfaceSection, parts);
			return true;
		}
		if (matchesLengthAndIdentifier(parts, 4, structIdentifier) && parts[1].equals(outputStructIdentifier)) {
			createOutputStruct(configInterfaceContainer, configInterfaceSection, parts);
			return true;
		}
		if (matchesLengthAndIdentifier(parts, 3, structIdentifier) && parts[1].equals(inputStructIdentifier)) {
			createInputStruct(configInterfaceContainer, configInterfaceSection, parts);
			return true;
		}
		if (matchesLengthAndIdentifier(parts, 2, globalHelpIdentifier)) {
			setGlobalHelpUrl(configInterfaceContainer, configInterfaceSection, parts);
			return true;
		}
		if (!line.equals("")) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Invalid declaration format");
		}
		if (line.equals("") && configInterfaceSection.containsAnyDefinition()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Unexpected empty line");
		}
		return false;
	}

	private ParsedConfigInterfaceJTO createConfigInterfaceModel(ConfigInterfaceContainer configInterfaceContainer) {
		ParsedConfigInterfaceJTO configInterfaceJTO = new ParsedConfigInterfaceJTO();
		configInterfaceJTO.setFunctions(configInterfaceContainer.getFunctions());
		configInterfaceJTO.setInputStructs(configInterfaceContainer.getInputStructs());
		configInterfaceJTO.setOutputStructs(configInterfaceContainer.getOutputStructs());
		return configInterfaceJTO;
	}

	private void setGlobalHelpUrl(ConfigInterfaceContainer configInterfaceContainer,
			ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		if (configInterfaceContainer.containsGlobalHelpUrl()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Duplicate global help url");
		}
		if (configInterfaceSection.containsAnyDefinition()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Unexpected global help url");
		}
		try {
			validateUrl(parts[1]);
		} catch (MalformedURLException e) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Invalid help url format");
		}
		configInterfaceContainer.setGlobalHelpUrl(parts[1]);
	}

	private void createDescription(ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		if (configInterfaceSection.containsDescription()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Duplicate description");
		}
		configInterfaceSection.setDescription(description(parts, 1));
	}

	private void createHelpUrl(ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		if (configInterfaceSection.containsHelpUrl()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Duplicate help url");
		}
		try {
			validateUrl(parts[1]);
		} catch (MalformedURLException e) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Invalid help url format");
		}
		configInterfaceSection.setHelpUrl(parts[1]);
	}

	private void createParameter(ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		String paramName = parts[1];
		String paramType = parts[2];
		validateItemFormat(paramName);
		validateItemFormat(paramType);
		if (configInterfaceSection.containsParameter(paramName)) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Duplicate definition of parameter \"" + paramName + "\"");
		}
		FunctionParameterJTO parameter = new FunctionParameterJTO();
		parameter.setName(paramName);
		parameter.setType(capitalize(paramType));
		parameter.setDescription(description(parts, 3));
		configInterfaceSection.addParameter(parameter);
	}

	private void createReturnData(ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		String type = parts[1];
		validateItemFormat(type);
		if (configInterfaceSection.containsReturnData()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Duplicate return definition");
		}
		FunctionReturnDataJTO returnData = new FunctionReturnDataJTO();
		returnData.setType(capitalize(type));
		returnData.setDescription(description(parts, 2));
		configInterfaceSection.setReturnData(returnData);
	}

	private void createFunction(ConfigInterfaceContainer configInterfaceContainer,
			ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		String functionName = parts[1];
		validateItemFormat(functionName);
		if (configInterfaceContainer.containsFunction(functionName)) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Duplicate declaration of function \"" + functionName + "\"");
		}
		if (!configInterfaceSection.containsOnlyFunctionDefinition()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Invalid definition of function \"" + functionName + "\"");
		}
		List<String> parameters = argumentList(parts, 2, true);
		if (!configInterfaceSection.parametersIn(parameters)) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Parameter list of function \"" + functionName + "\" does not match definition");
		}
		configInterfaceSection.addMissingParameters(parameters);
		FunctionJTO function = new FunctionJTO();
		function.setName(functionName);
		function.setDescription(configInterfaceSection.getDescription());
		function.setParameters(configInterfaceSection.getParameters());
		function.setReturnData(configInterfaceSection.getReturnData());
		function.setHelpUrl(configInterfaceSection.getHelpUrl());
		configInterfaceContainer.addFunction(function);
	}

	private void createInputStructField(ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		String type = parts[1];
		validateItemFormat(type);
		if (configInterfaceSection.containsInputStructField()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber, "Duplicate field definition");
		}
		InputStructFieldJTO inputStructField = new InputStructFieldJTO();
		inputStructField.setType(capitalize(type));
		inputStructField.setDescription(description(parts, 2));
		configInterfaceSection.setInputStructField(inputStructField);
	}

	private void createInputStruct(ConfigInterfaceContainer configInterfaceContainer,
			ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		String structField = parts[2];
		if (!configInterfaceSection.containsOnlyInputStructDefinition()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Invalid definition of object field \"" + structField + "\"");
		}
		List<InputStructPathFieldJTO> fieldsOnPath = inputStructFieldsOnPath(parts);
		if (configInterfaceSection.containsInputStructField()) {
			configInterfaceSection.getInputStructField().setFieldsOnPath(fieldsOnPath);
			configInterfaceSection.getInputStructField().setHelpUrl(configInterfaceSection.getHelpUrl());
		} else {
			InputStructFieldJTO inputStructField = new InputStructFieldJTO();
			inputStructField.setFieldsOnPath(fieldsOnPath);
			inputStructField.setHelpUrl(configInterfaceSection.getHelpUrl());
			configInterfaceSection.setInputStructField(inputStructField);
		}
		createInputStruct(configInterfaceSection, configInterfaceContainer, structField);
	}

	private void createInputStruct(ConfigInterfaceSection configInterfaceSection,
			ConfigInterfaceContainer configInterfaceContainer, String structField)
			throws InvalidConfigInterfaceFormatException {
		String structName = inputStructName(configInterfaceSection.getInputStructField().getFieldsOnPath());
		if (!configInterfaceContainer.containsInputStruct(structName)) {
			InputStructJTO inputStruct = new InputStructJTO();
			inputStruct.setName(structName);
			List<InputStructFieldJTO> inputStructFields = new LinkedList<>();
			inputStructFields.add(configInterfaceSection.getInputStructField());
			inputStruct.setFields(inputStructFields);
			configInterfaceContainer.addInputStruct(inputStruct);
		} else {
			if (configInterfaceContainer.containsInputStructField(structName,
					configInterfaceSection.getInputStructField().getFieldsOnPath())) {
				throw new InvalidConfigInterfaceFormatException(lineNumber,
						"Duplicate declaration of object field \"" + structField + "\"");
			}
			configInterfaceContainer.addInputStructField(structName, configInterfaceSection.getInputStructField());
		}
	}

	private List<InputStructPathFieldJTO> inputStructFieldsOnPath(String[] parts)
			throws InvalidConfigInterfaceFormatException {
		List<InputStructPathFieldJTO> fieldsOnPathList = new LinkedList<>();
		String[] fieldsOnPath = parts[2].split("\\.");
		for (String fieldOnPath : fieldsOnPath) {
			InputStructPathFieldJTO fieldOnPathJTO = new InputStructPathFieldJTO();
			String listSize = StringUtils.substringBetween(fieldOnPath, "[", "]");
			if (listSize == null) {
				validateItemFormat(fieldOnPath);
				fieldOnPathJTO.setName(fieldOnPath);
				fieldsOnPathList.add(fieldOnPathJTO);
			} else {
				if (!listSize.matches("[0-9]+") || listSize.equals("0")) {
					throw new InvalidConfigInterfaceFormatException(lineNumber,
							"Invalid index of object field \"" + fieldOnPath + "\"");
				}
				Integer intListSize = Integer.parseInt(listSize);
				String name = fieldOnPath.replace("[" + listSize + "]", "");
				validateItemFormat(name);
				fieldOnPathJTO.setName(name);
				fieldOnPathJTO.setListSize(intListSize);
				fieldsOnPathList.add(fieldOnPathJTO);
			}
		}
		return fieldsOnPathList;
	}

	private void createOutputStructField(ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		String fieldName = parts[1];
		String fieldType = parts[2];
		validateItemFormat(fieldName);
		validateItemFormat(fieldType);
		if (configInterfaceSection.containsOutputStructField(fieldName)) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Duplicate definition of field \"" + fieldName + "\"");
		}
		OutputStructFieldJTO outputStructField = new OutputStructFieldJTO();
		outputStructField.setName(fieldName);
		outputStructField.setType(capitalize(fieldType));
		outputStructField.setDescription(description(parts, 3));
		configInterfaceSection.addOutputStructField(outputStructField);
	}

	private void createOutputStruct(ConfigInterfaceContainer configInterfaceContainer,
			ConfigInterfaceSection configInterfaceSection, String[] parts)
			throws InvalidConfigInterfaceFormatException {
		String structName = parts[2];
		validateItemFormat(structName);
		if (configInterfaceContainer.containsOutputStruct(structName)) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Duplicate declaration of object \"" + structName + "\"");
		}
		if (!configInterfaceSection.containsOnlyOutputStructDefinition()) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Invalid definition of object \"" + structName + "\"");
		}
		List<String> fields = argumentList(parts, 3, true);
		if (!configInterfaceSection.fieldsIn(fields)) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Fields list of object \"" + structName + "\" does not match definition");
		}
		configInterfaceSection.addMissingFields(fields);
		OutputStructJTO struct = new OutputStructJTO();
		struct.setName(capitalize(structName));
		struct.setDescription(configInterfaceSection.getDescription());
		struct.setFields(configInterfaceSection.getOutputStructFields());
		struct.setHelpUrl(configInterfaceSection.getHelpUrl());
		configInterfaceContainer.addOutputStruct(struct);
	}

	private BufferedReader bufferedReader(byte[] configInterface) throws SQLException {
		InputStream configInterfaceInputStream = new ByteArrayInputStream(configInterface);
		return new BufferedReader(new InputStreamReader(configInterfaceInputStream));
	}

	private String removeExcessWhiteSpaces(String line) {
		return line.replaceAll("\\s+", " ").trim();
	}

	private String removeCommentPrefix(String line) {
		return line.replaceAll("^/+", "").trim();
	}

	private boolean matchesLengthAndIdentifier(String[] parts, Integer length, String identifier) {
		return parts.length >= length && parts[0].equals(identifier);
	}

	private String description(String[] parts, Integer descriptionIndex) throws InvalidConfigInterfaceFormatException {
		List<String> partsList = argumentList(parts, descriptionIndex, false);
		return String.join(" ", partsList);
	}

	private List<String> argumentList(String[] parts, Integer argumentsIndex, boolean validate)
			throws InvalidConfigInterfaceFormatException {
		List<String> partsList = new ArrayList<>(Arrays.asList(parts));
		for (int i = 0; i < argumentsIndex; i++) {
			partsList.remove(0);
		}
		if (validate) {
			for (String part : partsList) {
				validateItemFormat(part);
			}
		}
		return partsList;
	}

	private String capitalize(String string) {
		return StringUtils.capitalize(string);
	}

	private String inputStructName(List<InputStructPathFieldJTO> fieldsOnPath) {
		return capitalize(fieldsOnPath.get(0).getName());
	}

	private void validateItemFormat(String item) throws InvalidConfigInterfaceFormatException {
		if (item != null && !item.matches("(?i)[a-z][a-z0-9_]*")) {
			throw new InvalidConfigInterfaceFormatException(lineNumber,
					"Not allowed characters in item \"" + item + "\"");
		}
	}

	private void validateUrl(String url) throws MalformedURLException {
		new URL(url);
	}

}
