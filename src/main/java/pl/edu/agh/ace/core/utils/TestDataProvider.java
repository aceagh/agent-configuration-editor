package pl.edu.agh.ace.core.utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;

import pl.edu.agh.ace.core.configinterface.model.ConfigInterface;
import pl.edu.agh.ace.core.project.dao.ProjectDao;
import pl.edu.agh.ace.core.project.model.Project;

@Component
public class TestDataProvider {

	private static final List<String> emails = ImmutableList.of("vonszymon@gmail.com", "robert.maguda@gmail.com",
			"paweltopa@gmail.com");

	@Value(value = "classpath:example_interfaces/environment_config.ace")
	private Resource environmentConfig;

	@Value(value = "classpath:example_interfaces/foram_config.ace")
	private Resource foramConfig;

	@Value(value = "classpath:example_interfaces/simulation_config.ace")
	private Resource simulationConfig;

	@Autowired
	private ProjectDao projectDao;

	// uncomment to create example project at startup
	@PostConstruct
	public void insertTestData() throws IOException {
		try {
			for (String email : emails) {
				ConfigInterface configInterface = new ConfigInterface();
				configInterface.setName("Environment Config");
				byte[] content = IOUtils.toByteArray(environmentConfig.getInputStream());
				configInterface.setContent(content);

				ConfigInterface configInterface2 = new ConfigInterface();
				configInterface2.setName("Foram Config");
				byte[] content2 = IOUtils.toByteArray(foramConfig.getInputStream());
				configInterface2.setContent(content2);

				ConfigInterface configInterface3 = new ConfigInterface();
				configInterface3.setName("Simulation Config");
				byte[] content3 = IOUtils.toByteArray(simulationConfig.getInputStream());
				configInterface3.setContent(content3);

				Project project = new Project();
				project.setName("eVOLUTUS");
				project.setOwner(email);
				project.setConfigInterfaces(Arrays.asList(configInterface, configInterface2, configInterface3));
				projectDao.save(project);
			}
		} catch (Exception e) {
		}
	}
}
