package pl.edu.agh.ace.core.project.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.ace.core.project.model.Project;

@Transactional
public interface ProjectDao extends CrudRepository<Project, Long> {
	@Override
	public List<Project> findAll();

	public List<Project> findByOwner(String owner);

	public Project findById(long id);

}
