var editor = null;

$(document).ready(function() {
	editor = ace.edit("ace-editor");
	editor.setTheme("ace/theme/github");
	editor.getSession().setMode("ace/mode/ace_template");

	$('#template-editor-modal').on('show.bs.modal', function(e) {

		editor.resize();
		var $modal = $(this);
		if (e.relatedTarget.name != '') {
			// insert config into name input and textarea
			$('#teUpdate').css('display', 'inline');
			editConfig($modal, e.relatedTarget.name);
		} else {
			// create new config
			$('#teUpdate').css('display', 'none');
			createConfig($modal);
		}
		editor.resize();
	});

	$('#template-editor-modal').on('hide.bs.modal', function(e) {
		configEditorHideError();
		$("#template-editor-form").each(function() {
			this.reset();
		});
		editor.session.setValue('', -1);
		editor.gotoLine(1);
		editor.resize();
	});

});

var ce_configContent;
var ce_configName;

function editConfig(modal, name) {
	configInterface = loadedProjectModel.configInterfacesParsed.filter(function(config) {
		return config.name == name;
	})[0];

	ce_configContent = configInterface.content;
	ce_configName = configInterface.name;

	editor.session.setValue(configInterface.content, -1);
	editor.gotoLine(1);
	editor.resize();
	modal.find('#templateEditorName').attr("value", configInterface.name);
}

function createConfig(modal) {
	ce_configContent = '';
	ce_configName = '';

	editor.session.setValue('', -1);
	modal.find('#templateEditorName').attr("value", '');
	modal.find("#template-id").attr("value", -1);
}

$(function() {
	$("#teSaveAs").on('click', function(e) {
		configEditorHideError();

		// get form content
		var updatedName = $('#templateEditorName').val();
		var updatedContent = editor.getValue();

		// check if name changed
		var config = menuConfigInterfaceList.filter(function(config) {
			return config[1] == updatedName;
		})[0];

		if (config != undefined) {
			configEditorShowError("'" + updatedName + "' already exist. Please enter a different template name.");
			return;
		}

		// check if name not empty
		if (!updatedName) {
			configEditorShowError("Please enter a template name.");
			return;
		}

		var fd = prepareConfigFormData(updatedName, updatedContent);

		// parse config and save as new
		$.ajax({
			url : "/configInterface/parse",
			type : "POST",
			data : fd,
			enctype : 'multipart/form-data',
			processData : false,
			contentType : false,
			cache : false,
			success : function(parsedConfigInterface) {
				// success
				loadedProjectModel.configInterfacesParsed.push(parsedConfigInterface);
				menuConfigInterfaceList.push([ parsedConfigInterface.id, parsedConfigInterface.name ]);
				redrawConfigInterfaceList();
				$('#template-editor-modal').modal('hide');
			},
			error : function(exception) {
				// error
				configEditorShowError(exception.responseText);
			}
		});
	});
});

$(function() {
	$("#teUpdate").on(
			'click',
			function(e) {
				configEditorHideError();
				var updatedContent = editor.getValue();
				var fd = prepareConfigFormData(ce_configName, updatedContent);

				// parse config and save as new
				$.ajax({
					url : "/configInterface/parse",
					type : "POST",
					data : fd,
					enctype : 'multipart/form-data',
					processData : false,
					contentType : false,
					cache : false,
					success : function(parsedConfigInterface) {
						// success
						var index = -1;
						loadedProjectModel.configInterfacesParsed.forEach(function(entry) {
							if (entry.name == ce_configName) {
								index = loadedProjectModel.configInterfacesParsed.indexOf(entry);
							}
						});

						if (index != -1) {
							loadedProjectModel.configInterfacesParsed[index] = parsedConfigInterface;
						}

						if (selectedConfigInterfaceModel && ce_configName == selectedConfigInterfaceModel.name) {
							// clear Blockly workspace and toolbox
							blocklyWorkspace.clear();
							updateToolboxWithNewCategories([]);

							var configModel = loadedProjectModel.configInterfacesParsed.filter(function(config) {
								return config.name == ce_configName;
							});
							if (selectedConfigInterfaceModel) {
								selectedConfigInterfaceModel.state = getWorkspaceState();
							}
							selectedConfigInterfaceModel = configModel[0];
							if (selectedConfigInterfaceModel.state) {
								initWorkspaceStateFromModel(selectedConfigInterfaceModel.state,
										selectedConfigInterfaceModel.parsedConfigInterface);
							} else {
								initWorkspaceFromModel(selectedConfigInterfaceModel.parsedConfigInterface);
							}
							$(".hamburger").trigger("click");
						}

						$('#template-editor-modal').modal('hide');
					},
					error : function(exception) {
						// error
						configEditorShowError(exception.responseText);
					}
				});

			});
});

function prepareConfigFormData(name, content) {
	// create FormData
	var fd = new FormData();
	var file = new File([ content ], name);
	fd.append('uploadfile', file);
	fd.append('project-id', loadedProjectId);

	return fd;
}

// Error handling

function configEditorShowError(message) {
	$('#template-editor-modal').find('#template-editor-error').css("display", "block");
	$('#template-editor-modal').find('#template-editor-error').text(message);
}

function configEditorHideError() {
	$('#template-editor-modal').find('#template-editor-error').css("display", "none");
}

function openHelpWindow() {
	window.open("https://bitbucket.org/aceagh/agent-configuration-editor/wiki/TemplateFormat");
}
