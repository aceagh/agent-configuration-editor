/*-------------------------------*/
/*----------TOOLBOX--------------*/
/*-------------------------------*/

function updateToolboxWithNewCategories(xmlCategories) {
	var xmlToolbox = xmlToolboxHeader + xmlToolboxDefaultContent;
	for (var i = 0; i < xmlCategories.length; i++) {
		xmlToolbox += xmlCategories[i];
	}
	xmlToolbox += xmlToolboxEndingTag;
	blocklyWorkspace.updateToolbox(xmlToolbox);
	blocklyWorkspace.resize();
}

function createDefaultXmlToolboxText() {
	var xmlToolbox = xmlToolboxHeader + xmlToolboxDefaultContent + xmlToolboxEndingTag;
	return xmlToolbox;
}

function createXmlToolboxCategoryText(categoryName, colour, blockNames) {
	var xmlBlocks = createXmlToolboxBlocksText(blockNames);
	var xmlCategory = '<category name="' + categoryName + '" colour="' + colour + '">' + xmlBlocks + '</category>\n';
	return xmlCategory;
}

function createXmlToolboxBlocksText(blockNames) {
	var xmlBlocks = '';
	for (var i = 0; i < blockNames.length; i++) {
		xmlBlocks += '<block type="' + blockNames[i] + '"></block>\n';
	}
	return xmlBlocks;
}

/*-------------------------------*/
/*--------INPUT STRUCTS----------*/
/*-------------------------------*/

function createStructBlocks(inputStructs, outputStructs) {
	var xmlInputStructCategories = createInputStructBlocks(inputStructs);
	var xmlOutputStructCategories = createOutputStructBlocks(outputStructs);
	updateToolboxWithNewCategories(xmlInputStructCategories.concat(xmlOutputStructCategories));
}

function createInputStructBlocks(inputStructs) {
	var xmlCategories = [];
	for (var i = 0; i < inputStructs.length; i++) {
		xmlCategories.push(createInputStructFieldBlocks(inputStructs[i]));
	}
	if (xmlCategories.length > 0) {
		xmlCategories.push('<sep></sep>\n');
	}
	return xmlCategories;
}

// inputStruct = { name, fields }
function createInputStructFieldBlocks(inputStruct) {
	var blockNames = [];
	var fields = inputStruct.fields;
	for (var i = 0; i < fields.length; i++) {
		blockNames.push(createInputStructFieldBlock(fields[i]));
	}
	return createXmlToolboxCategoryText(inputStruct.name, '#A6935B', blockNames);
}

// inputStructField = { fieldsOnPath, description, type, helpUrl}
// fieldOnPath = { name, listSize }
// listSize = optional
// fieldsOnPath[lastIndex] = processedField
function createInputStructFieldBlock(inputStructField) {
	var fieldsOnPath = inputStructField.fieldsOnPath;
	var blockName = createInputStructFieldBlockName(fieldsOnPath);
	Blockly.Blocks[blockName] = {
		init : function() {
			this.setOutput(true, inputStructField.type);
			this.setColour(45);
			this.setCommentText(createInputStructFieldDescription(inputStructField));
			this.setHelpUrl(inputStructField.helpUrl);
			this.commentReadOnly = true;
			var dummyInput = this.appendDummyInput('STRUCT_VAR');
			for (var i = 0; i < fieldsOnPath.length; i++) {
				var name = fieldsOnPath[i].name;
				var listSize = fieldsOnPath[i].listSize;
				dummyInput = dummyInput.appendField(fieldsOnPath[i].name);
				if (listSize && listSize > 0) {
					dummyInput = dummyInput.appendField(new Blockly.FieldNumber(0, 0, listSize - 1), name + i);
				}
			}
		},
		onchange : function(event) {
			if ((event.type == Blockly.Events.MOVE || event.type == Blockly.Events.CHANGE || event.type == Blockly.Events.DELETE)
					&& !this.workspace.isDragging() && !this.isInFlyout) {
				var structName = this.type.split('-')[0];
				var topBlock = this.getRootBlock();
				if (topBlock.callType_ == 'procedures_callnoreturn' || topBlock.callType_ == 'procedures_callreturn') {
					if (topBlock.arguments_.includes(structName)) {
						this.setWarningText(null);
						this.setDisabled(false);
						return;
					}
				}
				this.setWarningText('This block may be used only inside functions with parameter: ' + structName);
				this.setDisabled(true);
			}
		}
	};
	createInputStructFieldGenerators(blockName);
	return blockName;
}

function createInputStructFieldBlockName(fieldsOnPath) {
	return fieldsOnPath.map(function(field) {
		return field.name;
	}).join("-");
}

function createInputStructFieldDescription(inputStructField) {
	return aceBlockDescriptionPrefix
			+ (inputStructField.type ? inputStructField.type + ' - ' + inputStructField.description : '');
}

/*-------------------------------*/
/*--------OUTPUT STRUCTS---------*/
/*-------------------------------*/

function createOutputStructBlocks(outputStructs) {
	var xmlCategories = [];
	for (var i = 0; i < outputStructs.length; i++) {
		var blockName = createOutputStructBlock(outputStructs[i]);
		xmlCategories.push(createXmlToolboxCategoryText(outputStructs[i].name, '#A65B5B', [ blockName ]))
	}
	return xmlCategories;
}

// outputStruct = { name, description, fields, helpUrl }
// field = { name, type, description }
function createOutputStructBlock(outputStruct) {
	var blockName = createOutputStructBlockName(outputStruct);
	Blockly.Blocks[blockName] = {
		init : function() {
			this.setOutput(true, outputStruct.name);
			this.setColour(0);
			this.setHelpUrl(outputStruct.helpUrl);
			this.setCommentText(createOutputStructDescription(outputStruct));
			this.commentReadOnly = true;
			var fields = outputStruct.fields;
			for (var i = 0; i < fields.length; i++) {
				this.appendValueInput(fields[i].name).setCheck(fields[i].type).appendField(fields[i].name);
			}
		}
	};
	createOutputStructGenerators(blockName);
	return blockName;
}

function createOutputStructBlockName(outputStruct) {
	return 'output_' + outputStruct.name.toLowerCase();
}

function createOutputStructDescription(outputStruct) {
	var fields = outputStruct.fields;
	var outputStructDescriptionParts = [];
	if (outputStruct.description) {
		outputStructDescriptionParts.push(outputStruct.description);
	}
	if (fields.length > 0) {
		var fieldsDescriptions = [];
		fieldsDescriptions.push('Fields:');
		for (var i = 0; i < fields.length; i++) {
			var name = fields[i].name;
			var type = fields[i].type ? fields[i].type : 'Unspecified';
			var description = fields[i].description ? ' - ' + fields[i].description : '';
			fieldsDescriptions.push(name + ':' + type + description);
		}
		outputStructDescriptionParts.push(fieldsDescriptions.join('\n'));
	}
	return aceBlockDescriptionPrefix + outputStructDescriptionParts.join('\n');
}

/*-------------------------------*/
/*----------FUNCTIONS------------*/
/*-------------------------------*/

function addFunctionBlocksToWorkspace(functions, columns, columnDistance, rowDistance) {
	var position = [ 0, 0 ];
	var lastColumnCoordinate = columns * columnDistance;
	for (var i = 0; i < functions.length; i++) {
		position[0] = (i * columnDistance) % lastColumnCoordinate;
		position[1] = Math.floor(i / columns) * rowDistance;
		addFunctionBlockToWorkspace(functions[i], position);
	}
	refreshWorkspace();
}

// func = { name, parameters, description, returnData, helpUrl}
function addFunctionBlockToWorkspace(func, position) {
	var block = blocklyWorkspace.newBlock('procedures_defreturn');
	block.setFieldValue(func.name, 'NAME');
	block.setDeletable(false);
	block.setEditable(false);
	block.setCommentText(createFunctionDescription(func));
	block.setHelpUrl(func.helpUrl);
	block.getInput('RETURN').setCheck(func.returnData ? func.returnData.type : null)
	block.moveBy(position[0], position[1]);
	for (var i = 0; i < func.parameters.length; i++) {
		block.arguments_.push(func.parameters[i].name);
	}
	block.updateParams_();
}

function createFunctionDescription(func) {
	var parameters = func.parameters;
	var returnData = func.returnData;
	var funcDescriptionParts = [];
	if (func.description) {
		funcDescriptionParts.push(func.description);
	}
	if (parameters.length > 0) {
		var parametersDescriptions = [];
		parametersDescriptions.push('Parameters:');
		for (var i = 0; i < parameters.length; i++) {
			var name = parameters[i].name;
			var type = parameters[i].type ? parameters[i].type : 'Unspecified';
			var description = parameters[i].description ? ' - ' + parameters[i].description : '';
			parametersDescriptions.push(name + ':' + type + description);
		}
		funcDescriptionParts.push(parametersDescriptions.join('\n'));
	}
	if (returnData) {
		funcDescriptionParts.push('Returns:\n' + returnData.type + ' - ' + returnData.description);
	}
	return aceBlockDescriptionPrefix + funcDescriptionParts.join('\n');
}

/*-------------------------------*/
/*-------CUSTOM VARIABLE---------*/
/*-------------------------------*/

function createInitVariableBlock() {
	Blockly.Blocks['variables_init'] = {
		init : function() {
			this.jsonInit({
				"message0" : 'init %1 to %2',
				"args0" : [ {
					"type" : "field_variable",
					"name" : "VAR",
					"variable" : Blockly.Msg.VARIABLES_DEFAULT_NAME
				}, {
					"type" : "input_value",
					"name" : "VALUE"
				} ],
				"previousStatement" : null,
				"nextStatement" : null,
				"colour" : Blockly.Blocks.variables.HUE,
				"tooltip" : "Initializes this variable with given value.",
				"helpUrl" : Blockly.Msg.VARIABLES_SET_HELPURL
			});
			this.contextMenuMsg_ = Blockly.Msg.VARIABLES_SET_CREATE_GET;
		},
		contextMenuType_ : 'variables_get',
		customContextMenu : Blockly.Blocks['variables_get'].customContextMenu
	};
	createInitVariableGenerators();
}

/*-------------------------------*/
/*-----------CONTROL-------------*/
/*-------------------------------*/

var blocklyWorkspace;

function updateDisplayedCode(event) {
	var languageDropdown = document.getElementById('languageDropdown');
	var languageSelection = languageDropdown.options[languageDropdown.selectedIndex].value;
	var codeDiv = document.getElementById('codeDiv');
	var codeHolder = document.createElement('pre');
	codeHolder.className = 'prettyprint but-not-that-pretty';
	var code = document.createTextNode(Blockly[languageSelection].workspaceToCode(blocklyWorkspace));
	codeHolder.appendChild(code);
	codeHolder.addEventListener("click", selectBlockOnFunctionClick);
	codeDiv.replaceChild(codeHolder, codeDiv.lastElementChild);
	prettyPrint();
	setClickableFunctions();
}

var functionDeclarations = [ 'function', 'def', 'void', 'dynamic' ];

function setClickableFunctions() {
	var candidateFunctions = $("pre .kwd").each(function() {
		if (functionDeclarations.indexOf($(this).text()) >= 0) {
			var functionName = $(this).next();
			functionName.css("cursor", "pointer");
			functionName.css("cursor", "hand");
		}
	});
}

function selectBlockOnFunctionClick(event) {
	var clickedText = event.target.textContent;
	if (!clickedText || functionDeclarations.indexOf(event.target.previousSibling.textContent) < 0) {
		return;
	}
	clickedText = clickedText.trim();
	var topBlocks = blocklyWorkspace.getTopBlocks();
	for (var i = 0; i < topBlocks.length; i++) {
		if ((topBlocks[i].callType_ == 'procedures_callnoreturn' || topBlocks[i].callType_ == 'procedures_callreturn')
				&& topBlocks[i].getFieldValue('NAME') == clickedText) {
			topBlocks[i].select();
			blocklyWorkspace.setScale(blocklyWorkspace.options.zoomOptions.startScale);
			var coords = topBlocks[i].getRelativeToSurfaceXY();
			var metrics = blocklyWorkspace.getMetrics();
			blocklyWorkspace.scrollbar.set(coords.x * blocklyWorkspace.scale - metrics.contentLeft - metrics.viewWidth
					* 0.2, coords.y * blocklyWorkspace.scale - metrics.contentTop - metrics.viewHeight * 0.3);
			return;
		}
	}
}

function refreshWorkspace() {
	var blocks = blocklyWorkspace.getAllBlocks();
	for (var i = 0; i < blocks.length; i++) {
		blocks[i].initSvg();
	}
	blocklyWorkspace.render();
	blocklyWorkspace.updateVariableList(false);
	blocklyWorkspace.zoomToFit();
}

function initWorkspaceFromModel(parsedTemplateModel) {
	blocklyWorkspace.clear();
	createStructBlocks(parsedTemplateModel.inputStructs, parsedTemplateModel.outputStructs);
	addFunctionBlocksToWorkspace(parsedTemplateModel.functions, 4, 600, 400);
}

function initWorkspaceStateFromModel(xmlState, parsedTemplateModel) {
	blocklyWorkspace.clear();
	createStructBlocks(parsedTemplateModel.inputStructs, parsedTemplateModel.outputStructs);
	loadSavedWorkspaceState(xmlState, parsedTemplateModel.functions);
}

function loadSavedWorkspaceState(xmlState, functions) {
	var xmlDom = Blockly.Xml.textToDom(xmlState);
	Blockly.Xml.domToWorkspace(xmlDom, blocklyWorkspace);
	blocklyWorkspace.zoomToFit();
	setFunctionsOutputChecks(functions);
}

function getWorkspaceState() {
	var xmlState = Blockly.Xml.workspaceToDom(blocklyWorkspace);
	return Blockly.Xml.domToPrettyText(xmlState);
}

function setFunctionsOutputChecks(functions) {
	var topBlocks = blocklyWorkspace.getTopBlocks();
	for (var i = 0; i < topBlocks.length; i++) {
		var block = topBlocks[i];
		if (block.callType_ == 'procedures_callreturn') {
			var matching_func = functions.filter(function(func) {
				return func.name == block.getFieldValue('NAME');
			});
			if (matching_func.length == 1) {
				block.getInput('RETURN')
						.setCheck(matching_func[0].returnData ? matching_func[0].returnData.type : null);
				block.setHelpUrl(matching_func[0].helpUrl);
			}
		}
	}
}

$(document).ready(function() {
	blocklyWorkspace = Blockly.inject('blocklyDiv', {
		toolbox : createDefaultXmlToolboxText(),
		comments : true,
		zoom : {
			controls : true,
			wheel : true,
			startScale : 1.0,
			maxScale : 3.0,
			minScale : 0.3,
			scaleSpeed : 1.2
		}
	});
	blocklyWorkspace.addChangeListener(updateDisplayedCode);
	createInitVariableBlock();
});