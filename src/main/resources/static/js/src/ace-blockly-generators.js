/*--------------------------------*/
/*---------CODE GENERATORS--------*/
/*--------------------------------*/

function createInitVariableGenerators() {
	Blockly.JavaScript['variables_init'] = function(block) {
		var argument0 = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ASSIGNMENT) || '0';
		var varName = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
		return 'var ' + varName + ' = ' + argument0 + ';\n';
	};
	Blockly.Python['variables_init'] = function(block) {
		var argument0 = Blockly.Python.valueToCode(block, 'VALUE', Blockly.Python.ORDER_NONE) || '0';
		var varName = Blockly.Python.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
		return varName + ' = ' + argument0 + '\n';
	};
	Blockly.PHP['variables_init'] = function(block) {
		var argument0 = Blockly.PHP.valueToCode(block, 'VALUE', Blockly.PHP.ORDER_ASSIGNMENT) || '0';
		var varName = Blockly.PHP.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
		return varName + ' = ' + argument0 + ';\n';
	};
	Blockly.Lua['variables_init'] = function(block) {
		var argument0 = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '0';
		var varName = Blockly.Lua.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
		return 'local ' + varName + ' = ' + argument0 + '\n';
	};
	Blockly.Dart['variables_init'] = function(block) {
		var argument0 = Blockly.Dart.valueToCode(block, 'VALUE', Blockly.Dart.ORDER_ASSIGNMENT) || '0';
		var varName = Blockly.Dart.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
		return 'var ' + varName + ' = ' + argument0 + ';\n';
	};
}

function generateInputStructFieldCode(block, prefix, objectAccessor) {
	var fieldRow = block.getInput('STRUCT_VAR').fieldRow;
	var variableParts = [];
	var i = 0;
	while (i < fieldRow.length - 1) {
		var field = fieldRow[i];
		var nextField = fieldRow[i + 1];
		if (nextField instanceof Blockly.FieldNumber) {
			variableParts.push(field.getText() + '[' + nextField.getText() + ']');
			i += 2;
		} else {
			variableParts.push(field.getText());
			i++;
		}
	}
	variableParts[0] = prefix + variableParts[0];
	var lastField = fieldRow[fieldRow.length - 1];
	if (lastField instanceof Blockly.FieldLabel) {
		variableParts.push(lastField.getText());
	}
	var code = variableParts.join(objectAccessor);
	return code;
}

function createInputStructFieldGenerators(blockName) {
	Blockly.JavaScript[blockName] = function(block) {
		var code = generateInputStructFieldCode(block, '', '.');
		return [ code, Blockly.JavaScript.ORDER_ATOMIC ];
	};
	Blockly.Python[blockName] = function(block) {
		var code = generateInputStructFieldCode(block, '', '.');
		return [ code, Blockly.Python.ORDER_ATOMIC ];
	};
	Blockly.PHP[blockName] = function(block) {
		var code = generateInputStructFieldCode(block, '$', '->');
		return [ code, Blockly.PHP.ORDER_ATOMIC ];
	};
	Blockly.Lua[blockName] = function(block) {
		var code = generateInputStructFieldCode(block, '', '.');
		return [ code, Blockly.Lua.ORDER_ATOMIC ];
	};
	Blockly.Dart[blockName] = function(block) {
		var code = generateInputStructFieldCode(block, '', '.');
		return [ code, Blockly.Dart.ORDER_ATOMIC ];
	};
}

function createOutputStructGenerators(blockName) {
	Blockly.JavaScript[blockName] = function(block) {
		var fieldCodes = [];
		var inputs = block.inputList;
		for (var i = 0; i < inputs.length; i++) {
			var fieldValueCode = Blockly.JavaScript.valueToCode(block, inputs[i].name, Blockly.JavaScript.ORDER_ATOMIC)
					|| 'null';
			var fieldName = inputs[i].fieldRow[0].getText();
			fieldCodes.push(fieldName + ' : ' + fieldValueCode);
		}
		var code = '{ ' + fieldCodes.join(', ') + ' }';
		return [ code, Blockly.JavaScript.ORDER_ATOMIC ];
	};
	Blockly.Python[blockName] = function(block) {
		var fieldCodes = [];
		var inputs = block.inputList;
		for (var i = 0; i < inputs.length; i++) {
			var fieldValueCode = Blockly.Python.valueToCode(block, inputs[i].name, Blockly.Python.ORDER_ATOMIC)
					|| 'None';
			var fieldName = inputs[i].fieldRow[0].getText();
			fieldCodes.push('\'' + fieldName + '\' : ' + fieldValueCode);
		}
		var code = 'type(\'\', (object,), { ' + fieldCodes.join(', ') + ' })()';
		return [ code, Blockly.Python.ORDER_ATOMIC ];
	};
	Blockly.PHP[blockName] = function(block) {
		var fieldCodes = [];
		var inputs = block.inputList;
		for (var i = 0; i < inputs.length; i++) {
			var fieldValueCode = Blockly.PHP.valueToCode(block, inputs[i].name, Blockly.PHP.ORDER_ATOMIC) || 'null';
			var fieldName = inputs[i].fieldRow[0].getText();
			fieldCodes.push('\'' + fieldName + '\' => ' + fieldValueCode);
		}
		var code = '(object)[ ' + fieldCodes.join(', ') + ' ]';
		return [ code, Blockly.PHP.ORDER_ATOMIC ];
	};
	Blockly.Lua[blockName] = function(block) {
		var fieldCodes = [];
		var inputs = block.inputList;
		for (var i = 0; i < inputs.length; i++) {
			var fieldValueCode = Blockly.Lua.valueToCode(block, inputs[i].name, Blockly.Lua.ORDER_ATOMIC) || 'nil';
			var fieldName = inputs[i].fieldRow[0].getText();
			fieldCodes.push(fieldName + ' = ' + fieldValueCode);
		}
		var code = '{ ' + fieldCodes.join(', ') + ' }';
		return [ code, Blockly.Lua.ORDER_ATOMIC ];
	};
	Blockly.Dart[blockName] = function(block) {
		var fieldCodes = [];
		var inputs = block.inputList;
		for (var i = 0; i < inputs.length; i++) {
			var fieldValueCode = Blockly.Dart.valueToCode(block, inputs[i].name, Blockly.Dart.ORDER_ATOMIC) || 'null';
			var fieldName = inputs[i].fieldRow[0].getText();
			fieldCodes.push('\'' + fieldName + '\' : ' + fieldValueCode);
		}
		var code = '{ ' + fieldCodes.join(', ') + ' }';
		return [ code, Blockly.Dart.ORDER_ATOMIC ];
	};
}