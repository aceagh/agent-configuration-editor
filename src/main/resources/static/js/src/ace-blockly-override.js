/*--------------------------------*/
/*-OVERRIDE BLOCKLY FUNCTIONALITY-*/
/*--------------------------------*/

var aceBlockDescriptionPrefix = '@ace@';

// new method for zooming with custom speed
Blockly.WorkspaceSvg.prototype.zoomWithSpeed = function(x, y, type, speed) {
	var metrics = this.getMetrics();
	var center = this.getParentSvg().createSVGPoint();
	center.x = x;
	center.y = y;
	center = center.matrixTransform(this.getCanvas().getCTM().inverse());
	x = center.x;
	y = center.y;
	var canvas = this.getCanvas();
	var scaleChange = (type == 1) ? speed : 1 / speed;
	var newScale = this.scale * scaleChange;
	if (newScale > this.options.zoomOptions.maxScale) {
		scaleChange = this.options.zoomOptions.maxScale / this.scale;
	} else if (newScale < this.options.zoomOptions.minScale) {
		scaleChange = this.options.zoomOptions.minScale / this.scale;
	}
	if (this.scale == newScale) {
		return;
	}
	if (this.scrollbar) {
		var matrix = canvas.getCTM().translate(x * (1 - scaleChange), y * (1 - scaleChange)).scale(scaleChange);
		this.scrollX = matrix.e - metrics.absoluteLeft;
		this.scrollY = matrix.f - metrics.absoluteTop;
	}
	this.setScale(newScale);
};

// disable variable getter block if it is not initialized
Blockly.Blocks['variables_get'] = {
	init : function() {
		this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
		this.setColour(Blockly.Blocks.variables.HUE);
		this.appendDummyInput().appendField(new Blockly.FieldVariable(Blockly.Msg.VARIABLES_DEFAULT_NAME), 'VAR');
		this.setOutput(true);
		this.setTooltip(Blockly.Msg.VARIABLES_GET_TOOLTIP);
		this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
	},
	contextMenuType_ : 'variables_set',

	customContextMenu : function(options) {
		var option = {
			enabled : true
		};
		var name = this.getFieldValue('VAR');
		option.text = this.contextMenuMsg_.replace('%1', name);
		var xmlField = goog.dom.createDom('field', null, name);
		xmlField.setAttribute('name', 'VAR');
		var xmlBlock = goog.dom.createDom('block', null, xmlField);
		xmlBlock.setAttribute('type', this.contextMenuType_);
		option.callback = Blockly.ContextMenu.callbackFactory(this, xmlBlock);
		options.push(option);
	},

	onchange : function(event) {
		if ((event.type == Blockly.Events.MOVE || event.type == Blockly.Events.CHANGE || event.type == Blockly.Events.DELETE)
				&& !this.workspace.isDragging() && !this.isInFlyout) {
			var variableName = this.getFieldValue('VAR');
			var rootBlock = this.getRootBlock();
			var parentBlock = this;
			while (parentBlock.getParent()) {
				var childBlock = parentBlock;
				parentBlock = parentBlock.getParent();
				if (isVariableSetter(parentBlock, variableName)) {
					this.setWarningText(null);
					this.setDisabled(false);
					return;
				}
				if (parentBlock.callType_ == 'procedures_callreturn') {
					if (parentBlock.getInput("RETURN").connection.targetBlock() == childBlock) {
						var statement = parentBlock.getInput("STACK").connection.targetBlock();
						while (statement) {
							if (isVariableSetter(statement, variableName)) {
								this.setWarningText(null);
								this.setDisabled(false);
								return;
							}
							statement = statement.getNextBlock();
						}
					}
				}
			}
			var rootIsFunction = rootBlock.callType_ == 'procedures_callnoreturn'
					|| rootBlock.callType_ == 'procedures_callreturn';
			if (rootIsFunction && rootBlock.arguments_.includes(variableName)) {
				this.setWarningText(null);
				this.setDisabled(false);
				return;
			}
			var topBlocks = blocklyWorkspace.getTopBlocks(true);
			for (var i = 0; i < topBlocks.length; i++) {
				var statement = topBlocks[i];
				if (rootBlock === statement && !rootIsFunction) {
					this.setWarningText('Initialize this variable before using it');
					this.setDisabled(true);
					return;
				}
				while (statement) {
					if ((statement.type == 'variables_set' || statement.type == 'variables_init')
							&& statement.getFieldValue('VAR') == variableName) {
						this.setWarningText(null);
						this.setDisabled(false);
						return;
					}
					statement = statement.getNextBlock();
				}
			}
			this.setWarningText('Initialize this variable before using it');
			this.setDisabled(true);
		}
	}
};

function isVariableSetter(block, variableName) {
	return (block.type == 'variables_set' || block.type == 'variables_init')
			&& (block.getFieldValue('VAR') == variableName);
}

// support for new block property 'commentReadOnly'
Blockly.Comment.prototype.setVisible = function(visible) {
	if (visible == this.isVisible()) {
		return;
	}
	Blockly.Events.fire(new Blockly.Events.Ui(this.block_, 'commentOpen', !visible, visible));
	if ((!this.block_.isEditable() && !this.textarea_) || goog.userAgent.IE || this.block_.commentReadOnly) {
		Blockly.Warning.prototype.setVisible.call(this, visible);
		return;
	}
	var text = this.getText();
	var size = this.getBubbleSize();
	if (visible) {
		this.bubble_ = new Blockly.Bubble((this.block_.workspace), this.createEditor_(), this.block_.svgPath_,
				this.iconXY_, this.width_, this.height_);
		this.bubble_.registerResizeEvent(this.resizeBubble_.bind(this));
		this.updateColour();
	} else {
		this.bubble_.dispose();
		this.bubble_ = null;
		this.textarea_ = null;
		this.foreignObject_ = null;
	}
	this.setText(text);
	this.setBubbleSize(size.width, size.height);
};

// styling of bubble warning text based on appended comment prefix
// 'aceBlockDescriptionPrefix'
Blockly.Warning.textToDom_ = function(text) {
	var cleanedText = text;
	var isAceBlockDescription = false;
	if (text.startsWith(aceBlockDescriptionPrefix)) {
		cleanedText = text.substring(aceBlockDescriptionPrefix.length);
		isAceBlockDescription = true;
	}
	var paragraph = (Blockly.createSvgElement('text', {
		'class' : 'blocklyText blocklyBubbleText',
		'y' : Blockly.Bubble.BORDER_WIDTH
	}, null));
	var lines = cleanedText.split('\n');
	for (var i = 0; i < lines.length; i++) {
		var isAceBlockDescriptionSection = isAceBlockDescription
				&& (lines[i] == 'Parameters:' || lines[i] == 'Returns:' || lines[i] == 'Fields:');
		var tspanStyle = isAceBlockDescriptionSection ? {
			'dy' : '1em',
			'x' : Blockly.Bubble.BORDER_WIDTH,
			'font-weight' : 'bold'
		} : {
			'dy' : '1em',
			'x' : Blockly.Bubble.BORDER_WIDTH
		};
		if (isAceBlockDescriptionSection && i != 0) {
			var tspanElement = Blockly.createSvgElement('tspan', tspanStyle, paragraph);
			var textNode = document.createTextNode(' ');
			tspanElement.appendChild(textNode);
		}
		var tspanElement = Blockly.createSvgElement('tspan', tspanStyle, paragraph);
		var textNode = document.createTextNode(lines[i]);
		tspanElement.appendChild(textNode);
	}
	return paragraph;
};

// adding new 'init' variable block to VARIABLES category in flyout
Blockly.Variables.flyoutCategory = function(workspace) {
	var variableList = workspace.variableList;
	variableList.sort(goog.string.caseInsensitiveCompare);

	var xmlList = [];
	var button = goog.dom.createDom('button');
	button.setAttribute('text', Blockly.Msg.NEW_VARIABLE);
	button.setAttribute('callbackKey', 'CREATE_VARIABLE');

	Blockly.registerButtonCallback('CREATE_VARIABLE', function(button) {
		Blockly.Variables.createVariable(button.getTargetWorkspace());
	});

	xmlList.push(button);

	if (variableList.length > 0) {
		if (Blockly.Blocks['variables_init']) {
			var block = goog.dom.createDom('block');
			block.setAttribute('type', 'variables_init');
			block.setAttribute('gap', 8);
			var field = goog.dom.createDom('field', null, variableList[0]);
			field.setAttribute('name', 'VAR');
			block.appendChild(field);
			xmlList.push(block);
		}
		if (Blockly.Blocks['variables_set']) {
			var block = goog.dom.createDom('block');
			block.setAttribute('type', 'variables_set');
			if (Blockly.Blocks['math_change']) {
				block.setAttribute('gap', 8);
			} else {
				block.setAttribute('gap', 24);
			}
			var field = goog.dom.createDom('field', null, variableList[0]);
			field.setAttribute('name', 'VAR');
			block.appendChild(field);
			xmlList.push(block);
		}
		if (Blockly.Blocks['math_change']) {
			var block = goog.dom.createDom('block');
			block.setAttribute('type', 'math_change');
			if (Blockly.Blocks['variables_get']) {
				block.setAttribute('gap', 20);
			}
			var value = goog.dom.createDom('value');
			value.setAttribute('name', 'DELTA');
			block.appendChild(value);

			var field = goog.dom.createDom('field', null, variableList[0]);
			field.setAttribute('name', 'VAR');
			block.appendChild(field);

			var shadowBlock = goog.dom.createDom('shadow');
			shadowBlock.setAttribute('type', 'math_number');
			value.appendChild(shadowBlock);

			var numberField = goog.dom.createDom('field', null, '1');
			numberField.setAttribute('name', 'NUM');
			shadowBlock.appendChild(numberField);

			xmlList.push(block);
		}

		for (var i = 0; i < variableList.length; i++) {
			if (Blockly.Blocks['variables_get']) {
				var block = goog.dom.createDom('block');
				block.setAttribute('type', 'variables_get');
				if (Blockly.Blocks['variables_set']) {
					block.setAttribute('gap', 8);
				}
				var field = goog.dom.createDom('field', null, variableList[i]);
				field.setAttribute('name', 'VAR');
				block.appendChild(field);
				xmlList.push(block);
			}
		}
	}
	return xmlList;
};

/*--------------------------------*/
/*------GENERATORS OVERRIDE-------*/
/*--------------------------------*/

// get rid of global variables automatic declaration
Blockly.JavaScript.init = function(workspace) {
	Blockly.JavaScript.definitions_ = Object.create(null);
	Blockly.JavaScript.functionNames_ = Object.create(null);

	if (!Blockly.JavaScript.variableDB_) {
		Blockly.JavaScript.variableDB_ = new Blockly.Names(Blockly.JavaScript.RESERVED_WORDS_);
	} else {
		Blockly.JavaScript.variableDB_.reset();
	}
};

Blockly.Python.init = function(workspace) {
	Blockly.Python.PASS = this.INDENT + 'pass\n';
	Blockly.Python.definitions_ = Object.create(null);
	Blockly.Python.functionNames_ = Object.create(null);

	if (!Blockly.Python.variableDB_) {
		Blockly.Python.variableDB_ = new Blockly.Names(Blockly.Python.RESERVED_WORDS_);
	} else {
		Blockly.Python.variableDB_.reset();
	}
};

Blockly.PHP.init = function(workspace) {
	Blockly.PHP.definitions_ = Object.create(null);
	Blockly.PHP.functionNames_ = Object.create(null);

	if (!Blockly.PHP.variableDB_) {
		Blockly.PHP.variableDB_ = new Blockly.Names(Blockly.PHP.RESERVED_WORDS_, '$');
	} else {
		Blockly.PHP.variableDB_.reset();
	}
};

Blockly.Dart.init = function(workspace) {
	Blockly.Dart.definitions_ = Object.create(null);
	Blockly.Dart.functionNames_ = Object.create(null);

	if (!Blockly.Dart.variableDB_) {
		Blockly.Dart.variableDB_ = new Blockly.Names(Blockly.Dart.RESERVED_WORDS_);
	} else {
		Blockly.Dart.variableDB_.reset();
	}
};

// get rid of generating comments
Blockly.JavaScript.scrub_ = function(block, code) {
	var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
	var nextCode = Blockly.JavaScript.blockToCode(nextBlock);
	return code + nextCode;
};

Blockly.Dart.scrub_ = function(block, code) {
	var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
	var nextCode = Blockly.Dart.blockToCode(nextBlock);
	return code + nextCode;
};

Blockly.Lua.scrub_ = function(block, code) {
	var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
	var nextCode = Blockly.Lua.blockToCode(nextBlock);
	return code + nextCode;
};

Blockly.PHP.scrub_ = function(block, code) {
	var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
	var nextCode = Blockly.PHP.blockToCode(nextBlock);
	return code + nextCode;
};

Blockly.Python.scrub_ = function(block, code) {
	var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
	var nextCode = Blockly.Python.blockToCode(nextBlock);
	return code + nextCode;
};

// get rid of generating 'global' statements inside functions
Blockly.Python['procedures_defreturn'] = function(block) {
	var funcName = Blockly.Python.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Procedures.NAME_TYPE);
	var branch = Blockly.Python.statementToCode(block, 'STACK');
	if (Blockly.Python.STATEMENT_PREFIX) {
		branch = Blockly.Python.prefixLines(Blockly.Python.STATEMENT_PREFIX.replace(/%1/g, '\'' + block.id + '\''),
				Blockly.Python.INDENT)
				+ branch;
	}
	if (Blockly.Python.INFINITE_LOOP_TRAP) {
		branch = Blockly.Python.INFINITE_LOOP_TRAP.replace(/%1/g, '"' + block.id + '"') + branch;
	}
	var returnValue = Blockly.Python.valueToCode(block, 'RETURN', Blockly.Python.ORDER_NONE) || '';
	if (returnValue) {
		returnValue = '  return ' + returnValue + '\n';
	} else if (!branch) {
		branch = Blockly.Python.PASS;
	}
	var args = [];
	for (var i = 0; i < block.arguments_.length; i++) {
		args[i] = Blockly.Python.variableDB_.getName(block.arguments_[i], Blockly.Variables.NAME_TYPE);
	}
	var code = 'def ' + funcName + '(' + args.join(', ') + '):\n' + branch + returnValue;
	code = Blockly.Python.scrub_(block, code);
	Blockly.Python.definitions_['%' + funcName] = code;
	return null;
};

Blockly.Python['procedures_defnoreturn'] = Blockly.Python['procedures_defreturn'];

Blockly.PHP['procedures_defreturn'] = function(block) {
	var funcName = Blockly.PHP.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Procedures.NAME_TYPE);
	var branch = Blockly.PHP.statementToCode(block, 'STACK');
	if (Blockly.PHP.STATEMENT_PREFIX) {
		branch = Blockly.PHP.prefixLines(Blockly.PHP.STATEMENT_PREFIX.replace(/%1/g, '\'' + block.id + '\''),
				Blockly.PHP.INDENT)
				+ branch;
	}
	if (Blockly.PHP.INFINITE_LOOP_TRAP) {
		branch = Blockly.PHP.INFINITE_LOOP_TRAP.replace(/%1/g, '\'' + block.id + '\'') + branch;
	}
	var returnValue = Blockly.PHP.valueToCode(block, 'RETURN', Blockly.PHP.ORDER_NONE) || '';
	if (returnValue) {
		returnValue = '  return ' + returnValue + ';\n';
	}
	var args = [];
	for (var i = 0; i < block.arguments_.length; i++) {
		args[i] = Blockly.PHP.variableDB_.getName(block.arguments_[i], Blockly.Variables.NAME_TYPE);
	}
	var code = 'function ' + funcName + '(' + args.join(', ') + ') {\n' + branch + returnValue + '}';
	code = Blockly.PHP.scrub_(block, code);
	Blockly.PHP.definitions_['%' + funcName] = code;
	return null;
};

Blockly.PHP['procedures_defnoreturn'] = Blockly.PHP['procedures_defreturn'];

// get rid of generating 'main' function
Blockly.Dart.finish = function(code) {
	var imports = [];
	var definitions = [];
	for ( var name in Blockly.Dart.definitions_) {
		var def = Blockly.Dart.definitions_[name];
		if (def.match(/^import\s/)) {
			imports.push(def);
		} else {
			definitions.push(def);
		}
	}
	delete Blockly.Dart.definitions_;
	delete Blockly.Dart.functionNames_;
	Blockly.Dart.variableDB_.reset();
	var allDefs = imports.join('\n') + '\n\n' + definitions.join('\n\n');
	return allDefs.replace(/\n\n+/g, '\n\n').replace(/\n*$/, '\n\n\n') + code;
};
