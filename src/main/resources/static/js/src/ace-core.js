/*-------------------------------*/
/*-----------HAMBURGER-----------*/
/*-------------------------------*/

$(document).ready(function() {
	var trigger = $('.hamburger'), overlay = $('.overlay'), isClosed = false;

	trigger.click(function() {
		hamburger_cross();
	});

	function hamburger_cross() {

		if (isClosed == true) {
			overlay.hide();
			trigger.removeClass('is-open');
			trigger.addClass('is-closed');
			isClosed = false;
		} else {
			overlay.show();
			trigger.removeClass('is-closed');
			trigger.addClass('is-open');
			isClosed = true;
		}
	}

	$('[data-toggle="offcanvas"]').click(function() {
		$('#wrapper').toggleClass('toggled');
	});
});

/*-------------------------------*/
/*---------- PROJECTS -----------*/
/*-------------------------------*/

var loadedProjectModel = null;
var loadedProjectId = -1;

$(document).ready(function() {
	selectedConfigInterfaceModel = null;
	showProjectView(false);
});

function showProjectView(show) {
	if (show == true) {
		$('#loadedProjectMenu').css('display', 'block');
		$('#selectProjectMenu').css('display', 'none');
	} else {
		$('#loadedProjectMenu').css('display', 'none');
		$('#selectProjectMenu').css('display', 'block');
	}
};

/*-------------------------------*/
/*-------- MENU ACTIONS ---------*/
/*-------------------------------*/

//-- PROJECT ACTIONS -- //
var menuProjectList = [];

//On page load read project list into array and redraw
$(document).ready(function() {
	$('#projectList li').each(function(element) {
		menuProjectList.push([ $(this).attr('id'), $(this).text() ]);
	});
	redrawProjectList();
});

//Sort project list and replace html inside ul
function redrawProjectList() {
	menuProjectList = menuProjectList.sort(ascSort);

	var projectListContent = '<li class="dropdown-header">Available projects</li>';
	menuProjectList
		.forEach(function(entry) {
			projectListContent += '\
		<li>\
			<div class="col-xs-6 element-text">\
				<a href="#" data-toggle="modal"\
				id="'
					+ entry[0]
					+ '" data-target="#load-project-modal">'
					+ entry[1]
					+ '</a>\
			</div>\
			<div class="col-xs-3 control-button">\
				<a href="#" data-toggle="modal" id="'
					+ entry[0]
					+ '" name="'
					+ entry[1]
					+ '"data-target="#rename-project-modal"><i\
					class="fa fa-i-cursor" title="Rename"></i></a>\
			</div>\
			<div class="col-xs-3 control-button">\
				<a href="#" data-toggle="modal" id="'
					+ entry[0]
					+ '" data-target="#delete-project-modal"><i\
					class="fa fa-trash" title="Delete"></i></a>\
			</div>\
		</li>'
	});
	$('#projectList').html(projectListContent);
};

//Create empty project - send request when button inside modal is pressed
$(function() {
	$("#createEmptyProjectButton").on('click', function(e) {
		e.preventDefault();
		$.ajax({
			url : "/project/createEmpty",
			type : "POST",
			data : $("#create-empty-project-form").serialize(),
			success : function(projectModel) {
				// success
				// response is a ProjectParsedConfig with no configs
				menuProjectList.push([ projectModel.id, projectModel.name ]);
				loadProject(projectModel);
			},
			error : function(request) {
				// error
				displayErrorModal("Unexpected error",
						"A following error was encountered:",
						"HTTP " + request.status + ": '" + request.statusText + "'");
			}
		});
	});
});

//Trigger click on file input element
$(function() {
	$("#importProject").on('click', function(e) {
		e.preventDefault();
		$("#import-project-input:hidden").trigger('click');
	});
});

//Import project

var importedProject = null;

//read file
$(document).ready(function() {
	$("#import-project-input").on("change", function(e) {
		var file = e.originalEvent.target.files[0];
		
		if(!file) {
			displayErrorModal("File error",
					"Unable to read file. Please try again.");
			return;
		}
		
		var reader = new FileReader();
	    reader.readAsText(file, "UTF-8");
	    var content = "";
	    
		reader.onload = function (evt) {
			importedProject = evt.target.result;
			// show modal with project name
			$('#import-project-modal').modal('show');
	    }
		
	    reader.onerror = function (evt) {
			displayErrorModal("Import error",
				"Unable to import project.");
	    	importedProject = null;
			$("#import-project-form").each(function() {
				this.reset();
			});
	    }
	});
});

//get project new name
$(function() {
	$("#importNameButton").on('click', function(e) {
		e.preventDefault();

		// get project name from form
		var parsedImportedProject;
		try {
			parsedImportedProject = JSON.parse(importedProject);
		} catch(e) {
			displayErrorModal("Import error",
				"Unable to import project. Make sure this is a valid ACE project format.");
			return;
		}
		parsedImportedProject.name = $("#import-project-form #project-import-input").val();
		
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			url : "/project/import",
			type : "POST",
			data : JSON.stringify(parsedImportedProject),
			success : function(response) {
				// success
				loadProject(response);
			},
			error : function(request) {
				// error
				displayErrorModal("Unexpected error",
						"A following error was encountered:",
						"HTTP " + request.status + ": '" + request.statusText + "'");
			}
		});
		
		importedProject = null;
		$("#import-project-form").each(function() {
			this.reset();
		});
	
	});
});

//Load project

//Set project id in hidden input form, so it can be send in request
$(document).ready(function() {
	$('#load-project-modal').on('show.bs.modal', function(e) {
		var $modal = $(this);
		$modal.find("#project-id").val(e.relatedTarget.id);
	});
});

//Load project on modal button pressed
$(function() {
	$("#loadProjectButton").on('click', function(e) {
		var projectId = $("#load-project-form #project-id").val();

		$.ajax({
			url : "/project/get",
			type : "POST",
			data : {
				"project-id" : projectId
			},
			success : function(projectModel) {
				// success
				loadProject(projectModel);
			},
			error : function(request) {
				// error
				displayErrorModal("Unexpected error",
						"A following error was encountered:",
						"HTTP " + request.status + ": '" + request.statusText + "'");
			}
		});
	});
});

//Rename project

//Set project id in hidden input form, so it can be send in request
//and set input value to old name
$(document).ready(function() {
	$('#rename-project-modal').on('show.bs.modal', function(e) {
		var $modal = $(this);
		$modal.find("#project-id").val(e.relatedTarget.id);
		$("#project-rename-input").attr("value", e.relatedTarget.name);
	});
});

//Send project rename request
$(function() {
	$("#renameProjectButton").on('click', function(e) {
		$.ajax({
			url : "/project/rename",
			type : "POST",
			data : $("#rename-project-form").serialize(),
			success : function(project) {
				// success
				renameListElement(menuProjectList, project.id, project.name);
				redrawProjectList();
			},
			error : function(request) {
				// error
				displayErrorModal("Unexpected error",
						"A following error was encountered:",
						"HTTP " + request.status + ": '" + request.statusText + "'");
			}
		});
		// reseting modal form
		$("#rename-project-form").each(function() {
			this.reset();
		});
	});
});

//Delete project

//Set project id in hidden input form, so it can be send in request
$(document).ready(function() {
	$('#delete-project-modal').on('show.bs.modal', function(e) {
		var $modal = $(this);
		$modal.find("#project-id").val(e.relatedTarget.id);
	});
});

//Send delete project request
$(function() {
	$("#deleteProjectButton").on('click', function(e) {
		$.ajax({
			url : "/project/delete",
			type : "POST",
			data : $("#delete-project-form").serialize(),
			success : function(response) {
				// success
				deleteListElement(menuProjectList, response.id);
				redrawProjectList();
			},
			error : function(request) {
				// error
				displayErrorModal("Unexpected error",
						"A following error was encountered:",
						"HTTP " + request.status + ": '" + request.statusText + "'");
			}
		});
	});
});

//Loading project
//Used when user creates new project or load existing one
function loadProject(project) {
	loadedProjectId = project.id;
	loadedProjectModel = project;

	// Fill config interface list from project model
	menuConfigInterfaceList = [];
	if (project.configInterfacesParsed != null) {
		for (var int = 0; int < project.configInterfacesParsed.length; int++) {
			menuConfigInterfaceList.push([ project.configInterfacesParsed[int].id,
				project.configInterfacesParsed[int].name ]);
		}
	}

	// Set project name label
	$('#project-name-label').text("Project: " + project.name);
	redrawConfigInterfaceList();
	showProjectView(true);
}

//Saving project
$(function() {
	$("#saveProjectButton").on('click', function(e) {
		saveProject();
	});
});

function saveProject() {
	setExitDialog(false);
	projectModel = getProjectModel();

	$.ajax({
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		url : "/project/save",
		type : "POST",
		data : JSON.stringify(projectModel),
		success : function(response) {
			// success
			loadProject(response);
		},
		error : function(request) {
			// error
			displayErrorModal("Unexpected error",
					"Unable to save project. Please export your project to file or your changes will be lost.");
		}
	});
}

//Download code
$(function() {
	$("#downloadCode").on('click', function(e) {
		if(loadedProjectModel.configInterfacesParsed.length < 1) {
			displayErrorModal("Nothing to download",
			"There are no templates in project.");
			return;
		}
		
		// save current state
		if (selectedConfigInterfaceModel) {
			selectedConfigInterfaceModel.state = getWorkspaceState();
		}

		var zip = new JSZip();
		loadedProjectModel.configInterfacesParsed.forEach(function(configInterface) {
			if (configInterface.state) {
				initWorkspaceStateFromModel(configInterface.state,
						configInterface.parsedConfigInterface);
			} else {
				initWorkspaceFromModel(configInterface.parsedConfigInterface);
			}

			var languageDropdown = document.getElementById('languageDropdown');
			var languageSelection = languageDropdown.options[languageDropdown.selectedIndex].value;
			var code = document.createTextNode(Blockly[languageSelection].workspaceToCode(blocklyWorkspace));

			var fileExtension = "";
			switch(languageSelection) {
			case "JavaScript":
				fileExtension = "js";
				break;
			case "Python":
				fileExtension = "py";
				break;
			case "PHP":
				fileExtension = "php";
				break;
			case "Lua":
				fileExtension = "lua";
				break;
			case "Dart":
				fileExtension = "dart";
				break;
			}

			zip.file(configInterface.name.replace(/ /g,"_")+"."+fileExtension, code.textContent);
		});

		// generate zip file and trigger downloading
		zip.generateAsync({type:"blob"})
		.then(function(content) {
			saveAs(content, loadedProjectModel.name.replace(/ /g,"_")+"_ConfigurationFiles.zip");
		});

		if(selectedConfigInterfaceModel) {
			if (selectedConfigInterfaceModel.state) {
				initWorkspaceStateFromModel(selectedConfigInterfaceModel.state,
						selectedConfigInterfaceModel.parsedConfigInterface);
			} else {
				initWorkspaceFromModel(selectedConfigInterfaceModel.parsedConfigInterface);
			}
		} else {
			blocklyWorkspace.clear();
			updateToolboxWithNewCategories([]);
		}

	});
});

//Export project
$(function() {
	$("#exportProject").on('click', function(e) {
		project = getProjectModel();
		// owner, name and id will be overwritten during import
		var fileName = project.name+".aceproj";
		project.owner = "undefined";
		project.name = "undefined";
		project.id = 0;
		var blob = new Blob([JSON.stringify(project)], {type: "application/octet-stream"});
		saveAs(blob, fileName);
	});
});

//Close project - discard changes
$(function() {
	$("#discardAndCloseProjectButton").on('click', function(e) {
		closeProject();
	});
});

//Close project - save changes
$(function() {
	$("#saveAndCloseProjectButton").on('click', function(e) {
		projectModel = getProjectModel();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			url : "/project/save",
			type : "POST",
			data : JSON.stringify(projectModel),
			success : function(response) {
				// success
				closeProject();
			},
			error : function(request) {
				// error
				displayErrorModal("Unexpected error",
						"A following error was encountered:",
						"HTTP " + request.status + ": '" + request.statusText + "'");
			}
		});
	});
});

function updateProjectList() {
	$.ajax({
		url : "/project/getAll",
		type : "GET",
		success : function(projectList) {
			// success
			menuProjectList = []
			projectList.forEach(function(project) {
				menuProjectList.push([ project.id, project.name ]);
			});
			redrawProjectList();
		},
		error : function(request) {
			// error
			displayErrorModal("Unexpected error",
					"A following error was encountered:",
					"HTTP " + request.status + ": '" + request.statusText + "'");
		}
	});
}

function closeProject() {
	setExitDialog(false);

	// unload project
	loadedProjectModel = null;
	loadedProjectId = -1;

	// deselect active config interface
	selectedConfigInterfaceModel = -1;

	// clear Blockly workspace and toolbox
	blocklyWorkspace.clear();
	updateToolboxWithNewCategories([]);

	// download, redraw and display project list
	updateProjectList();
	showProjectView(false);
}

//Returns project model without parsed config interfaces
function getProjectModel() {
	if (selectedConfigInterfaceModel) {
		selectedConfigInterfaceModel.state = getWorkspaceState();
	}

	var projectModel = jQuery.extend(true, {}, loadedProjectModel);

	projectModel.configInterfacesParsed.forEach(function(config) {
		delete config["parsedConfigInterface"];
	});

	projectModel["configInterfaces"] = projectModel["configInterfacesParsed"];
	delete projectModel["configInterfacesParsed"];

	return projectModel;
}

//-- CONFIG INTERFACE ACTIONS -- //

var menuConfigInterfaceList = [];
var selectedConfigInterfaceModel = -1;

//On page load read interface list into array and redraw
$(document).ready(function() {
	$('#templateList li').each(function(element) {
		menuConfigInterfaceList.push([ $(this).attr('id'), $(this).text() ]);
	});
	redrawConfigInterfaceList();
});

//Sort interface list and replace html inside ul
function redrawConfigInterfaceList() {
	menuConfigInterfaceList = menuConfigInterfaceList.sort(ascSort);

	var configInterfaceListContent = '<li class="dropdown-header">Available templates</li>';
	menuConfigInterfaceList
	.forEach(function(entry) {
		configInterfaceListContent += '\
			<li>\
			<div class="col-xs-6 element-text template-name">\
				<a href="#" id="'
					+ entry[0]
					+ '">'
					+ entry[1]
					+ '</a>\
			</div>\
			<div class="col-xs-2 control-button">\
				<a href="#" data-toggle="modal" id="'
					+ entry[0]
					+ '" name="'
					+ entry[1]
					+ '"data-target="#template-editor-modal"><i\
					class="fa fa-code" title="Edit"></i></a>\
			</div>\
			<div class="col-xs-2 control-button">\
				<a href="#" data-toggle="modal" id="'
					+ entry[0]
					+ '" name="'
					+ entry[1]
					+ '"data-target="#rename-template-modal"><i\
					class="fa fa-i-cursor" title="Rename"></i></a>\
			</div>\
			<div class="col-xs-2 control-button">\
				<a href="#" data-toggle="modal" id="'
					+ entry[0]
					+ '" name="'
					+ entry[1]
					+ '" data-target="#delete-template-modal"><i\
					class="fa fa-trash" title="Delete"></i></a>\
			</div>\
		</li>'
	});
	$('#templateList').html(configInterfaceListContent);
	setOnConfigInterfaceClickListener();
};

//Upload config interface

//Trigger click on file input element
$(function() {
	$("#uploadTemplate").on('click', function(e) {
		e.preventDefault();
		$("#upload-template-input:hidden").trigger('click');
	});
});

var MAX_FILESIZE = 65536;

$(document).ready(function() {
	$("#upload-template-input").on("change", function(e) {
		var file = e.originalEvent.target.files[0];
		if(file.size > MAX_FILESIZE) {
			displayErrorModal("File is too large", "Unable to upload template. File '"+file.name+"' has exceeded the maximum allowed size ("+MAX_FILESIZE+" B).");
			return;
		}
		
		var fd = new FormData($("#upload-template-form")[0]);
		fd.append('project-id', loadedProjectId);
		
		$.ajax({
			url : "/configInterface/parse",
			type : "POST",
			data : fd,
			enctype : 'multipart/form-data',
			processData : false,
			contentType : false,
			cache : false,
			success : function(parsedConfigInterface) {
				// success
				loadedProjectModel.configInterfacesParsed.push(parsedConfigInterface);
				menuConfigInterfaceList.push([ parsedConfigInterface.id, parsedConfigInterface.name ]);
				redrawConfigInterfaceList();
			},
			error : function(exception) {
				// error
				displayErrorModal("Parsing error", "Unable to parse template. Fix errors and try again:", exception.responseText);
			}
		});
		$("#upload-template-form").each(function() {
			this.reset();
		});
	});
});

//Error modal for general errors
function displayErrorModal(title, description, message) {
	$('#parser-error-modal').modal('show');
	$('#parser-error-modal').find('#error-title').text(title);
	if(message == undefined){
		$('#parser-error-modal').find('.modal-body').html("<p class=\"text-danger\">" + description + "</p>");
	}else {
		$('#parser-error-modal').find('.modal-body').html(
				"<p class=\"text-danger\">" + description + "</p>" + "<code>" + message + "</code>");
	}
}

//Rename config interface

//Set config interface id in hidden input form, so it can be send in request
//and set input value to old name
$(document).ready(function() {
	$('#rename-template-modal').on('show.bs.modal', function(e) {
		var $modal = $(this);
		$modal.find("#template-id").val(e.relatedTarget.id);
		$("#template-rename-input").attr("value", e.relatedTarget.name);
	});
});

//Rename config interface locally
$(function() {
	$("#renameTemplateButton").on('click', function(e) {
		var oldName = $("#template-rename-input").attr("value");

		var id = $("#rename-template-form #template-id").val();
		var newName = $("#rename-template-form #template-rename-input").val();

		renameConfigInterface(oldName, newName);
		redrawConfigInterfaceList();

		setExitDialog(true);
	});
});

//Delete config interface

//Set config interface id in hidden input form, so it can be send in request
//and set input value to old name
$(document).ready(function() {
	$('#delete-template-modal').on('show.bs.modal', function(e) {
		var $modal = $(this);
		$modal.find("#template-id").val(e.relatedTarget.id);
		$("#template-id").attr("value", e.relatedTarget.name);
	});
});

//Delete config interface locally
$(function() {
	$("#deleteTemplateButton").on('click', function(e) {
		var name = $("#template-id").attr("value");
		deleteConfigIntefaceListElement(name);
		redrawConfigInterfaceList();

		if (name == selectedConfigInterfaceModel.name) {
			// deleting active config interface
			// clear Blockly workspace and toolbox
			blocklyWorkspace.clear();
			updateToolboxWithNewCategories([]);
		}

		setExitDialog(true);
	});
});

//On config interface click listener
function setOnConfigInterfaceClickListener() {
	$(".template-name").on(
			'click',
			function(event) {
				var configName = $(this).children().first().text();
				var configModel = loadedProjectModel.configInterfacesParsed.filter(function(config) {
					return config.name == configName;
				});
				if (selectedConfigInterfaceModel && selectedConfigInterfaceModel.name == configModel[0].name) {
					return;
				}
				highlightSelectedItem(event);
				if (selectedConfigInterfaceModel) {
					selectedConfigInterfaceModel.state = getWorkspaceState();
				}
				selectedConfigInterfaceModel = configModel[0];
				if (selectedConfigInterfaceModel.state) {
					initWorkspaceStateFromModel(selectedConfigInterfaceModel.state,
							selectedConfigInterfaceModel.parsedConfigInterface);
				} else {
					initWorkspaceFromModel(selectedConfigInterfaceModel.parsedConfigInterface);
				}
				$(".hamburger").trigger("click");
			});
}

function highlightSelectedItem(event) {
	var liElement = $(event.target).parent().parent();
	var ulElement = liElement.parent();
	ulElement.children().removeClass('highlighted-item');
	liElement.addClass('highlighted-item');
}

//Comparator used for sorting config interface and state list
function ascSort(a, b) {
	return b[1] < a[1] ? 1 : -1;
}

//rename config interface list element
function renameConfigInterface(oldName, newName) {
	if (oldName == newName) {
		return;
	}
	menuConfigInterfaceList.forEach(function(entry) {
		if (entry[1] == oldName) {
			entry[1] = newName;
		}
	});

	loadedProjectModel.configInterfacesParsed.forEach(function(entry) {
		if (entry.name == oldName) {
			entry.name = newName;
		}
	});
}

function setExitDialog(show) {
	if (show) {
		$(window).bind('beforeunload', function() {
			// Not all browsers support custom popup message
			return 'Are you sure you want to leave?';
		});
	} else {
		$(window).unbind('beforeunload');
	}
}

function deleteConfigIntefaceListElement(name) {
	var indexToRemove = -1;
	for (var i = 0; i < menuConfigInterfaceList.length; i++) {
		if (menuConfigInterfaceList[i][1] == name) {
			indexToRemove = i;
			break;
		}
	}
	if (indexToRemove != -1) {
		menuConfigInterfaceList.splice(indexToRemove, 1);
	}

	indexToRemove = 0;
	loadedProjectModel.configInterfacesParsed.forEach(function(entry) {
		if (entry.name == name) {
			loadedProjectModel.configInterfacesParsed.splice(indexToRemove, 1);
		}
		indexToRemove++;
	});
}

//delete state or interface list element
function deleteListElement(list, id) {
	var indexToRemove = -1;
	for (var i = 0; i < list.length; i++) {
		if (list[i][0] == id) {
			indexToRemove = i;
			break;
		}
	}
	if (indexToRemove != -1) {
		list.splice(indexToRemove, 1);
	}
}

function renameListElement(list, id, newName) {
	list.forEach(function(entry) {
		if (entry[0] == id) {
			entry[1] = newName;
		}
	});
}