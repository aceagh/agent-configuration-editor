#!/bin/bash

# -- VARIABLES -- #

if [ "$(whoami)" != "root" ]; then
  echo "Need to be root"
  exit 1
fi

# # Default variables
postgres_pwd="abc123"

# Help printout
read -r -d '' helpmsg << EOM
Script for MySQL database configuration.
    -p | --password  database user password (default 'abc123')
    -h | --help for  print this description.
EOM

# -- PARSING ARGUMENTS -- #

while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -p|--password)
            dbpwd=$key
            shift
            ;;
        -h|--help)
            echo "$helpmsg"
            exit 1
            ;;
        *)
            echo "Unknown option " $key
            exit 1
            ;;
    esac
    shift
done

# -- POSTGRESQL INSTALLATION -- #

dpkg --get-selections | grep postgresql-contrib > /dev/null
serverinstalled=$?
dpkg --get-selections | grep postgresql-client> /dev/null
clientinstalled=$?

if [ $serverinstalled -eq 1 -o $clientinstalled -eq 1 ] ; then
  printf "PostgreSQL not installed. Installing... \n"
  apt-get update
  apt-get install -y postgresql postgresql-contrib
fi

sudo -u postgres psql -U postgres -d postgres -c "create database ace;"
sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password '$postgres_pwd';"
